# Yalantis Health Report

App for QA Reports Generation.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites / environment requirements

- [Node.js](https://nodejs.org/en/) v16.15.0 or newer ([nvm](https://github.com/nvm-sh/nvm) usage is preferred).
- [npm](https://www.npmjs.com/) package manager.

## Installing / Getting started

1. Clone the repository and move to the project dir (for this step you may need [setup SSH keys](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)):

```bash
git clone https://gitlab.com/mariia.halkinaa1/yh-frontend yh-frontend
cd yh-frontend/
```

or

```bash
git clone git@gitlab.com:mariia.halkinaa1/yh-frontend.git yh-frontend
cd yh-frontend/
```

2. Install `node modules` dependencies:

```bash
npm i
```

3. Create a .env file at the root directory of your application by copying the .env.example file. Add variables to it.

```bash
cp .env.example .env
```

4. Now you can run the app in the development mode using `npm run start` command.

5. Run `npm run configure-husky` to configure pre-commit checks.

## Predefined package.json scripts

In the project directory, you can run:

| Script                    | Description                                                                                                                                                                         |
|---------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `npm run start`           | Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.                                                                |
| `npm run build`           | Builds the app for production to the `build` folder.                                                                                                                                |
| `npm run test`            | Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information. |
| `npm run format`          | Run Prettier formatter                                                                                                                                                              |
| `npm run lint`            | Run ESLint checks                                                                                                                                                                   |
| `npm run lint:fix`        | Run ESLint to find and fix problems                                                                                                                                                 |
| `npm run configure-husky` | Run `husky install` command and configure pre-commit checks.                                                                                                                        |


## Developing

### Built With

| Module                           | Technologies                                                                                                |
|----------------------------------|-------------------------------------------------------------------------------------------------------------|
| Core                             | React `16.8.0` and ReactDOM `16.8.0`                                                                        |
| Typing                           | TypeScript `^4.9.x`                                                                                         |
| Navigation                       | React Router `^6.x.x`                                                                                       |
| HTTP-client                      | Axios `^1.2.x`                                                                                              |
| Cache manager                    | React Query `^3.x.x`                                                                                        |
| Components UI library            | [@atlaskit](https://atlassian.design/components)                                                            |
| Forms                            | [Formik](https://formik.org/docs/overview)                                                                  |
| Fields parsing and validation    | [Yup](https://github.com/jquense/yup)                                                                       |
| Code formatters and code linters | ESLint, Prettier                                                                                            |
| Git hooks and git linters        | husky, lint-staged                                                                                          |
| Testing                          | [Jest](https://jestjs.io/docs/getting-started), [@testing-library/react](https://testing-library.com/docs/) |
