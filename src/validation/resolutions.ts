import * as Yup from 'yup';

const resolutionsValidationSchema = Yup.object().shape({
  resolutions: Yup.array().of(
    Yup.string().required('Please select a resolution')
  )
});

export default resolutionsValidationSchema;
