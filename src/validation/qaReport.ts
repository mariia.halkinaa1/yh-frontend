import * as Yup from 'yup';

import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXT_FIELD } from './constants';

const sprint = { sprint: Yup.string().required('Please select a sprint') };
const start = {
  start: Yup.date()
    .required('Please select start date and time')
    .max(new Date(), 'Start date should be before today')
};
const end = {
  end: Yup.date()
    .required('Please select end date and time')
    .when('start', {
      is: (value: string) => !!value,
      then: Yup.date().min(
        Yup.ref('start'),
        'End date should be after start date'
      )
    })
};
const platforms = {
  platforms: Yup.array().of(
    Yup.object().shape({
      platform: Yup.string()
        .max(256, MAX_LENGTH_INPUT)
        .required('Please enter the platform name'),
      build: Yup.string().max(256, MAX_LENGTH_INPUT)
    })
  )
};

const sprintReportValidationSchema = Yup.object().shape({
  ...sprint,
  ...start,
  ...end,
  codefreeze: Yup.boolean(),
  codefreezeDate: Yup.date()
    .when('codefreeze', {
      is: true,
      then: Yup.date().required('Please select codefreeze date and time')
    })
    .when('start', {
      is: (value: string) => !!value,
      then: Yup.date().min(
        Yup.ref('start'),
        'Codefreeze date should be between start and end'
      )
    })
    .when('end', {
      is: (value: string) => !!value,
      then: Yup.date().max(
        Yup.ref('end'),
        'Codefreeze date should be between start and end'
      )
    }),
  ...platforms,
  testRail: Yup.array().of(
    Yup.object().shape({
      testRun: Yup.string().max(256, MAX_LENGTH_INPUT),
      testRailUrl: Yup.string()
        .required('Required field. Please enter a link to Test Rail')
        .url('Enter a valid link')
        .max(256, MAX_LENGTH_INPUT)
    })
  ),
  problems: Yup.string().max(512, MAX_LENGTH_TEXT_FIELD)
});

const deliveryReportValidationSchema = Yup.object().shape({
  ...sprint,
  ...start,
  ...end,
  deliveryDate: Yup.date().required('Please select delivery date and time'),
  ...platforms,
  releaseNotes: Yup.string().max(512, MAX_LENGTH_TEXT_FIELD)
});

export { sprintReportValidationSchema, deliveryReportValidationSchema };
