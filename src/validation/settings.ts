import * as Yup from 'yup';

import { JIRA_LINK, MAX_LENGTH_INPUT, REQUIRED_FIELD } from './constants';

const settingsValidationSchema = Yup.object().shape({
  jira_link: Yup.string()
    .required('Link should be filled')
    .url('Enter a valid link')
    .test(
      'Jira link',
      "Enter link in the format 'https://example.atlassian.net'",
      (link) => {
        const regex = new RegExp(JIRA_LINK);
        return !!link?.match(regex);
      }
    )
    .max(256, MAX_LENGTH_INPUT),
  jira_token: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required('Token should be filled'),
  jira_time_zone: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required('Time zone should be filled'),
  jira_email: Yup.string()
    .email('Enter a valid email')
    .max(256, MAX_LENGTH_INPUT),
  jira_project_key: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_bug_status: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_delivered_stories_statuses: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_status_open: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_status_close: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_status_reopen: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_priority_high: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_priority_medium: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_issue_priority_low: Yup.string()
    .max(256, MAX_LENGTH_INPUT)
    .required(REQUIRED_FIELD),
  jira_team_frontend: Yup.string().max(256, MAX_LENGTH_INPUT),
  jira_team_backend: Yup.string().max(256, MAX_LENGTH_INPUT),
  jira_team_ios: Yup.string().max(256, MAX_LENGTH_INPUT),
  jira_team_android: Yup.string().max(256, MAX_LENGTH_INPUT)
});

export default settingsValidationSchema;
