export const JIRA_LINK = /https:\/\/[a-z]+[-a-z]+[a-z]+\.atlassian\.net\/*$/gi;

export const MAX_LENGTH_INPUT = 'This field must be at most 256 characters';

export const MAX_LENGTH_TEXT_FIELD =
  'This field must be at most 512 characters';

export const REQUIRED_FIELD = 'This field should be filled';
