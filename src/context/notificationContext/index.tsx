import React, { useState } from 'react';

export enum TYPE {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR'
}

export interface INotificationData {
  title: string;
  description?: string;
}

interface INotificationContext {
  type: TYPE | null;
  notification: INotificationData;
  onSuccess: (data: INotificationData) => void;
  onError: (data: INotificationData) => void;
}

const defaultValue: INotificationContext = {
  type: null,
  notification: { title: '' },
  onSuccess: () => {},
  onError: () => {}
};

const NotificationContext =
  React.createContext<INotificationContext>(defaultValue);

const NotificationProvider = ({ children }: { children: JSX.Element }) => {
  const [type, setType] = useState<TYPE | null>(defaultValue.type);
  const [notification, setNotification] = useState<INotificationData>(
    defaultValue.notification
  );

  const onClear = () => {
    setTimeout(() => {
      setNotification(defaultValue.notification);
      setType(defaultValue.type);
    }, 5000);
  };

  const onSuccess = (data: INotificationData) => {
    setNotification(data);
    setType(TYPE.SUCCESS);

    onClear();
  };

  const onError = (data: INotificationData) => {
    setNotification(data);
    setType(TYPE.ERROR);

    onClear();
  };

  return (
    <NotificationContext.Provider
      value={{ type, notification, onSuccess, onError }}
    >
      {children}
    </NotificationContext.Provider>
  );
};

export { NotificationProvider };
export default NotificationContext;
