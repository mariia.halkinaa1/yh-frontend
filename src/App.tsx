import React from 'react';
import { Routes, Route } from 'react-router-dom';

import PATH from './constants/routes';
import PrivateRoute from './components/PrivateRoute';
import Notification from './components/Notification';
import Auth from './pages/Auth';
import Main from './pages/Main';
import { useAuth } from './hooks';

function App() {
  const { isAuth } = useAuth();
  return (
    <>
      <Routes>
        {!isAuth && <Route path={PATH.LOGIN} element={<Auth />} />}
        <Route
          path="*"
          element={
            <PrivateRoute isAuth={isAuth}>
              <Main />
            </PrivateRoute>
          }
        />
      </Routes>
      <Notification />
    </>
  );
}

export default App;
