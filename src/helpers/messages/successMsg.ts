const successMessages = {
  UpdateSettings: {
    title: 'Settings has been successfully updated'
  }
};

export default successMessages;
