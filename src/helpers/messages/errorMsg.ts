const errorMessages = {
  ErrorGoogleLogin: {
    title: 'Google login error',
    description: 'Google login error'
  },
  DRTChartScreenshot: {
    title: 'Unable to screenshot DRT Chart'
  }
};

export default errorMessages;
