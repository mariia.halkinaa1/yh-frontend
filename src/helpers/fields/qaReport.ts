const projectKey = {
  projectKey: {
    name: 'projectKey',
    label: 'Project key',
    placeholder: ''
  }
};
const sprint = {
  sprint: {
    name: 'sprint',
    label: 'Sprint',
    isRequired: true
  }
};
const start = {
  start: {
    name: 'start',
    label: 'Start date and time',
    isRequired: true
  }
};
const end = {
  end: {
    name: 'end',
    label: 'End date and time',
    isRequired: true
  }
};
const platform = {
  platform: {
    name: 'platform',
    type: 'text',
    label: 'Platform',
    placeholder: 'Android/iOS Client App, Admin panel, etc.',
    isRequired: true
  }
};
const build = {
  build: {
    name: 'build',
    type: 'text',
    label: 'Build number',
    placeholder: 'Number of the delivery candidate build'
  }
};

const sprintReportFields = {
  ...projectKey,
  ...sprint,
  ...start,
  ...end,
  codefreeze: {
    name: 'codefreeze',
    label: 'Codefreeze was'
  },
  codefreezeDate: {
    name: 'codefreezeDate',
    label: 'Codefreeze date and time',
    isRequired: true
  },
  ...platform,
  ...build,
  testRun: {
    name: 'testRun',
    type: 'text',
    label: 'Milestone/TestRun',
    placeholder: 'Enter a description of a Milestone/TestRun'
  },
  testRailUrl: {
    name: 'testRailUrl',
    type: 'text',
    label: 'Test Rail Link',
    placeholder: 'Add a link to Test Rail',
    isRequired: true
  },
  problems: {
    name: 'problems',
    type: 'text',
    label: 'Summary',
    placeholder: 'Describe main problems during the sprint',
    helperMsg:
      'Describe the overall results of the iteration, problems, any additional info you want to highlight'
  }
};

const deliveryReportFields = {
  ...projectKey,
  ...sprint,
  ...start,
  ...end,
  deliveryDate: {
    name: 'deliveryDate',
    label: 'Delivery date and time',
    isRequired: true
  },
  ...platform,
  ...build,
  releaseNotes: {
    name: 'releaseNotes',
    type: 'text',
    label: 'Release Notes',
    placeholder: 'Add release notes for this report'
  }
};

export { sprintReportFields, deliveryReportFields };
