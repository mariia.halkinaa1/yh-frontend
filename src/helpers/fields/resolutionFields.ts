const resolutionFields = {
  resolution: {
    name: 'resolution',
    label: 'Resolution',
    isRequired: true,
    placeholder: 'Select Resolution'
  }
};

export default resolutionFields;
