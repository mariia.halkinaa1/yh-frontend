const settingsFields = {
  jira_link: {
    name: 'jira_link',
    type: 'text',
    label: 'Link to Jira',
    placeholder: 'Enter link to Jira',
    isRequired: true
  },
  jira_token: {
    name: 'jira_token',
    type: 'text',
    label: 'Token',
    placeholder: 'Token for authorization',
    isRequired: true,
    helperMsg: 'How to generate JIRA token?'
  },
  jira_time_zone: {
    name: 'jira_time_zone',
    type: 'text',
    label: 'Jira time zone',
    placeholder: 'Select Jira time zone',
    isRequired: true
  },
  jira_email: {
    name: 'jira_email',
    type: 'text',
    label: "Client's Jira email",
    placeholder: "Enter client's Jira email",
    isRequired: false
  },
  defect_removal_trend: {
    name: 'defect_removal_trend',
    label: 'Latest amount of sprints for Defect Removal Trend'
  },
  jira_project_key: {
    name: 'jira_project_key',
    label: 'Project Key',
    placeholder: 'Enter Project Key',
    helperMsg: 'Enter project keys separated by commas',
    isRequired: true
  },
  jira_bug_status: {
    name: 'jira_bug_status',
    label: 'Bug status',
    placeholder: 'Enter bug status',
    helperMsg: 'Enter bug statuses separated by commas',
    isRequired: true
  },
  jira_delivered_stories_statuses: {
    name: 'jira_delivered_stories_statuses',
    label: 'Delivered story status',
    placeholder: 'Enter delivered story status',
    helperMsg: 'Enter delivered story statuses separated by commas',
    isRequired: true
  },
  jira_issue_status_open: {
    name: 'jira_issue_status_open',
    label: 'Open',
    placeholder: 'Enter Jira status',
    helperMsg: 'Enter Jira statuses separated by commas',
    isRequired: true
  },
  jira_issue_status_close: {
    name: 'jira_issue_status_close',
    label: 'Close',
    placeholder: 'Enter Jira status',
    helperMsg: 'Enter Jira statuses separated by commas',
    isRequired: true
  },
  jira_issue_status_reopen: {
    name: 'jira_issue_status_reopen',
    label: 'Reopen',
    placeholder: 'Enter Jira status',
    helperMsg: 'Enter Jira statuses separated by commas',
    isRequired: true
  },
  jira_issue_priority_high: {
    name: 'jira_issue_priority_high',
    label: 'High',
    placeholder: 'Enter Jira priority',
    helperMsg: 'Enter Jira priorities separated by commas',
    isRequired: true
  },
  jira_issue_priority_medium: {
    name: 'jira_issue_priority_medium',
    label: 'Medium',
    placeholder: 'Enter Jira priority',
    helperMsg: 'Enter Jira priorities separated by commas',
    isRequired: true
  },
  jira_issue_priority_low: {
    name: 'jira_issue_priority_low',
    label: 'Low',
    placeholder: 'Enter Jira priority',
    helperMsg: 'Enter Jira priorities separated by commas',
    isRequired: true
  },
  jira_team_frontend: {
    name: 'jira_team_frontend',
    label: 'Frontend',
    placeholder: 'Enter team member',
    helperMsg: 'Enter team members separated by commas',
    isRequired: false
  },
  jira_team_backend: {
    name: 'jira_team_backend',
    label: 'Backend',
    placeholder: 'Enter team member',
    helperMsg: 'Enter team members separated by commas',
    isRequired: false
  },
  jira_team_ios: {
    name: 'jira_team_ios',
    label: 'iOS',
    placeholder: 'Enter team member',
    helperMsg: 'Enter team members separated by commas',
    isRequired: false
  },
  jira_team_android: {
    name: 'jira_team_android',
    label: 'Android',
    placeholder: 'Enter team member',
    helperMsg: 'Enter team members separated by commas',
    isRequired: false
  }
};

export default settingsFields;
