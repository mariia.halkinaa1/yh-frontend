import React, { useEffect, useMemo } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';

import Preview from '../../components/Preview';
import Spinner from '../../components/Spinner';
import { ReportType } from '../../constants/generateReport';
import PATH from '../../constants/routes';
import { useReport } from '../../hooks';
import { formatDateAndTime } from '../../utils/formatDate';

import styles from './style.module.scss';

function ReportPreview() {
  const { reportId } = useParams();
  const { state } = useLocation();
  const navigate = useNavigate();

  const { isLoading, report } = useReport(reportId as string);
  const { projectName } = useMemo(
    () => ({
      projectName: state?.projectName
    }),
    [state]
  );

  const { isDeliveryReport, reporterName, ...data } = useMemo(
    () => ({
      isDeliveryReport: report?.report_type === ReportType.DELIVERY_REPORT,
      jiraProjectKey: report?.project_key,
      sprintNumber: report?.sprint_number,
      sprintStartDate: formatDateAndTime(report?.sprint_start_date),
      sprintEndDate: formatDateAndTime(report?.sprint_end_date),
      codefreezeDate: formatDateAndTime(report?.code_freeze_date),
      deliveryDate: formatDateAndTime(report?.delivery_date),
      problems: report?.problems,
      releaseNotes: report?.release_notes,
      reporterName: report?.reporter_name,
      listOfBugs: report?.report_data.list_of_bugs,
      deliveredStories: report?.report_data.delivered_stories,
      bugDynamics: report?.report_data.bugs_dynamics_during_the_sprint,
      bugStatistic: report?.report_data.bug_statistic_at_the_end_of_the_sprint,
      resolutionsByPlatforms: report?.resolutions_by_platforms,
      testRailData: report?.testrail_data
    }),
    [report]
  );

  useEffect(() => {
    if (!reportId || !state) navigate(PATH.PROJECTS);
  }, [navigate, reportId, state]);

  return isLoading ? (
    <Spinner />
  ) : (
    <>
      <div className={styles.report_preview__header}>
        <h2>
          {isDeliveryReport
            ? `QA Delivery report for ${projectName} project`
            : `QA Sprint report for ${projectName} project`}
        </h2>
        <div>{`Created by ${reporterName}`}</div>
      </div>
      <Preview data={data} isDeliveryReport={isDeliveryReport} />
    </>
  );
}

export default ReportPreview;
