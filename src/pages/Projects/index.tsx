import React, { useEffect } from 'react';
import { useQueryClient } from 'react-query';

import { ProjectsTable } from '../../components/Tables';
import { QUERY_KEYS } from '../../hooks/constants';

import styles from './style.module.scss';

function Projects() {
  const queryClient = useQueryClient();

  useEffect(() => {
    queryClient.removeQueries(QUERY_KEYS.PROJECT);
    queryClient.removeQueries(QUERY_KEYS.REPORTS);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={styles.projects}>
      <ProjectsTable />
    </div>
  );
}

export default Projects;
