import React, { useCallback, useMemo } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { BackButton } from '../../components/Buttons';
import Tabs from '../../components/Tabs';
import { SprintReportForm, DeliveryReportForm } from '../../components/Forms';
import Spinner from '../../components/Spinner';
import GenerateReportError from '../../components/ErrorMessages/GenerateReportError';
import PATH from '../../constants/routes';
import {
  useProjectKeys,
  useGenerationAvailable,
  useProject
} from '../../hooks';

import styles from './styles.module.scss';

function Report() {
  const navigate = useNavigate();
  const { id: projectId } = useParams();

  const { isLoading, isGenerationAvailable } = useGenerationAvailable();
  const { projectKeys } = useProjectKeys(isGenerationAvailable || false);
  const { project } = useProject(projectId as string);

  const projectKey = useMemo(() => project?.project_key, [project]);
  const projectName = useMemo(
    () =>
      projectKeys?.find(({ project_key }) => project_key === projectKey)
        ?.name || '',
    [projectKey, projectKeys]
  );
  const projectKeysOptions = useMemo(
    () =>
      projectKey && projectName
        ? [
            {
              label: `${projectKey} (${projectName})`,
              value: projectKey
            }
          ]
        : [
            {
              label: '',
              value: ''
            }
          ],
    [projectKey, projectName]
  );

  const sprintReportUrl = useMemo(
    () => `${PATH.PROJECT}/${projectId}${PATH.SPRINT_REPORT}`,
    [projectId]
  );
  const deliveryReportUrl = useMemo(
    () => `${PATH.PROJECT}/${projectId}${PATH.DELIVERY_REPORT}`,
    [projectId]
  );

  const tabs = useMemo(
    () => [
      {
        label: 'Sprint Report',
        url: sprintReportUrl,
        content: (
          <SprintReportForm
            projectKey={projectKey}
            projectName={projectName}
            projectKeysOptions={projectKeysOptions}
          />
        )
      },
      {
        label: 'Delivery Report',
        url: deliveryReportUrl,
        content: (
          <DeliveryReportForm
            projectKey={projectKey}
            projectName={projectName}
            projectKeysOptions={projectKeysOptions}
          />
        )
      }
    ],
    [
      projectKey,
      projectName,
      sprintReportUrl,
      deliveryReportUrl,
      projectKeysOptions
    ]
  );

  const goBackToProjectPage = useCallback(() => {
    navigate(`${PATH.PROJECT}/${projectId}`);
  }, [navigate, projectId]);

  return isLoading ? (
    <Spinner />
  ) : (
    <>
      <BackButton onClick={goBackToProjectPage} />
      <>
        {!isGenerationAvailable && (
          <div className={styles.generate_report_error}>
            <GenerateReportError />
          </div>
        )}
        <Tabs
          id="generate-report-tabs"
          tabs={tabs}
          defaultPath={deliveryReportUrl}
        />
      </>
    </>
  );
}

export default Report;
