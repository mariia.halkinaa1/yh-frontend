import React, { useMemo } from 'react';

import Tabs from '../../components/Tabs';
import PATH from '../../constants/routes';
import Projects from '../Projects';
import Settings from '../Settings';

function Dashboard() {
  const tabs = useMemo(
    () => [
      {
        label: 'Projects',
        url: PATH.PROJECTS,
        content: <Projects />
      },
      {
        label: 'Settings',
        url: PATH.SETTINGS,
        content: <Settings />
      }
    ],
    []
  );

  return <Tabs id="dashboard-tabs" tabs={tabs} defaultPath={PATH.PROJECTS} />;
}

export default Dashboard;
