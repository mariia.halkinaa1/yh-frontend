import React from 'react';

import SettingsForm from '../../components/Forms/SettingsForm';

import styles from './style.module.scss';

function Settings() {
  return (
    <div className={styles.settings}>
      <div className={styles.settings__title}>Jira Source for QA reports</div>
      <SettingsForm />
    </div>
  );
}

export default Settings;
