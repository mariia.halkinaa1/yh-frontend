import React, { lazy, Suspense } from 'react';

import Spinner from '../../components/Spinner';

const PageTemplate = lazy(() => import('../../components/PageTemplate'));

const Main = () => {
  return (
    <Suspense fallback={<Spinner />}>
      <PageTemplate />
    </Suspense>
  );
};

export default Main;
