import React, { useCallback, useMemo } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { Button, BackButton } from '../../components/Buttons';
import { GenerateReportError } from '../../components/ErrorMessages';
import { ReportsTable } from '../../components/Tables';
import { useGenerationAvailable, useProject } from '../../hooks';
import PATH from '../../constants/routes';

import styles from './style.module.scss';

function Project() {
  const navigate = useNavigate();
  const { id: projectId } = useParams();
  const { isLoading, isGenerationAvailable } = useGenerationAvailable();
  const { project } = useProject(projectId as string);

  const { projectKey, projectJiraLink } = useMemo(
    () => ({
      projectKey: project?.project_key,
      projectJiraLink: project?.jira_link
    }),
    [project]
  );

  const handleGenerateReport = useCallback(() => {
    navigate(`${PATH.PROJECT}/${projectId}${PATH.SPRINT_REPORT}`);
  }, [navigate, projectId]);

  const goBackToProjectsPage = useCallback(() => {
    navigate(PATH.PROJECTS);
  }, [navigate]);

  return (
    <div className={styles.project}>
      <div className={styles.project___header}>
        <BackButton onClick={goBackToProjectsPage} />
        <div className={styles.project__title}>QA Sprint Reports</div>
        <Button
          type="button"
          appearance="primary"
          onClick={handleGenerateReport}
          isDisabled={isLoading || !isGenerationAvailable}
        >
          Generate Report
        </Button>
        {!isLoading && isGenerationAvailable && (
          <>
            <div>
              Sprint report used for generating and storing data from report.
            </div>
            <div> Delivery report used for generating report.</div>
          </>
        )}
        {!isLoading && !isGenerationAvailable && <GenerateReportError />}
      </div>
      <ReportsTable projectKey={projectKey} projectJiraLink={projectJiraLink} />
    </div>
  );
}

export default Project;
