import React from 'react';
import { GoogleOAuthProvider } from '@react-oauth/google';

import { APP_NAME } from '../../constants/global';
import THEME_COLOR from '../../constants/themeColor';
import { LoginButton } from '../../components/Buttons';

import styles from './style.module.scss';

function Auth() {
  return (
    <GoogleOAuthProvider
      clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID as string}
    >
      <div
        className={styles.auth_container}
        style={{ backgroundColor: THEME_COLOR.PRIMARY }}
      >
        <div
          className={styles.auth_box}
          style={{ backgroundColor: THEME_COLOR.SECONDARY }}
        >
          <h1 className={styles.auth_box__title}>
            <b>{APP_NAME}</b>
          </h1>
          <LoginButton />
        </div>
      </div>
    </GoogleOAuthProvider>
  );
}

export default Auth;
