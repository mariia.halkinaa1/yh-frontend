const API_ENDPOINTS = {
  AUTH: {
    LOG_IN: '/auth/login/',
    LOG_OUT: '/auth/logout/',
    REFRESH_TOKEN: '/auth/refresh/'
  },
  REPORTS: '/reports/',
  REPORT: (id: string) => `/report/${id}/`,
  GENERATE_REPORT: (id: string) => `/generate_report/${id}/`,
  GENERATION_AVAILABLE: '/generation_available/',
  SETTINGS: '/settings/',
  SETTINGS_TIMEZONES: '/settings_timezones/',
  PROJECT_KEYS: '/project_keys/',
  PROJECTS: '/projects/',
  PROJECT: (id: string) => `/project/${id}/`,
  DRT: (id: string) => `/drt/${id}/`
};

export default API_ENDPOINTS;
