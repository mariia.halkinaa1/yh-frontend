import axios from 'axios';

const baseURL = process.env.REACT_APP_API_HOST;
const apiClient = axios.create({
  baseURL
});

export default apiClient;
