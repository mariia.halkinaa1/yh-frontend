export interface IAuthRequest {
  tokenId: string;
}

export interface IAuthResponse {
  access: string;
  refresh: string;
}

export interface IRefreshTokenRequest {
  refreshToken: string;
}

export interface IRefreshTokenResponse {
  access: string;
}
