import apiClient from '../index';
import API_ENDPOINTS from '../endpoints';

import {
  IAuthRequest,
  IAuthResponse,
  IRefreshTokenRequest,
  IRefreshTokenResponse
} from './types';

const logIn = async ({ tokenId }: IAuthRequest) => {
  const { data } = await apiClient.post<IAuthResponse>(
    API_ENDPOINTS.AUTH.LOG_IN,
    {
      token_id: tokenId
    }
  );
  return data;
};

const logOut = async () => {
  const { data } = await apiClient.post(API_ENDPOINTS.AUTH.LOG_OUT);
  return data;
};

const refreshAccessToken = async ({ refreshToken }: IRefreshTokenRequest) => {
  const { data } = await apiClient.post<IRefreshTokenResponse>(
    API_ENDPOINTS.AUTH.REFRESH_TOKEN,
    { refresh: refreshToken }
  );
  return data;
};

export default {
  logIn,
  logOut,
  refreshAccessToken
};
