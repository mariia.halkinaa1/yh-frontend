import { ReportType } from '../../constants/generateReport';

export type TReportData = {
  [key: string]: string;
}[];

export type TReportDataResponse = {
  data: TReportData;
  total?: number;
  textError: string;
};

export type TBugStatistic = {
  render_data_priority: TReportData;
  render_data_severity: TReportData;
  textError: string;
};

export type TBugDynamics = {
  data: {
    [key: string]: { [key: string]: string };
  };
  total?: number;
  textError: string;
};

export type IResolutionsByPlatforms = {
  platform: string;
  build_number?: string;
  resolution?: string;
}[];

export type ITestrailData = {
  testrail_url: string;
  test_run?: string;
}[];

export interface IQAReport {
  id: string;
  report_type: ReportType;
  publish_date: string;
  project_key: string;
  sprint_number: string;
  sprint_start_date: string;
  sprint_end_date: string;
  delivery_date: string;
  code_freeze_date: string;
  reporter_name: string;
  pdf_url: string;
  problems: string;
  release_notes: string;
  resolutions_by_platforms: IResolutionsByPlatforms;
  testrail_data: ITestrailData;
  report_data: {
    list_of_bugs: TReportDataResponse;
    delivered_stories: TReportDataResponse;
    bugs_dynamics_during_the_sprint: TBugDynamics;
    bug_statistic_at_the_end_of_the_sprint: TBugStatistic;
  };
}

export interface IGenerationAvailableResponse {
  generation_available: boolean;
}

export type IGetProjectKeysResponse = {
  id: string;
  project_key: string;
  name: string;
}[];

export interface IPostReportRequest {
  report_type: ReportType;
  project_key: string;
  sprint_number: string;
  sprint_start_date: string;
  sprint_end_date: string;
  code_freeze_date?: string;
  delivery_date?: string;
  resolutions_by_platforms: IResolutionsByPlatforms;
  testrail_data?: ITestrailData;
  problems?: string;
  release_notes?: string;
}

export interface IGenerateReportRequest {
  id: string;
  resolutions_by_platforms: string;
  file?: File;
}
