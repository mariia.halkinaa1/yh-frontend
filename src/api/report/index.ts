import axios from 'axios';

import {
  IGenerationAvailableResponse,
  IGetProjectKeysResponse,
  IPostReportRequest,
  IGenerateReportRequest,
  IQAReport
} from './types';
import apiClient from '../index';

import API_ENDPOINTS from '../endpoints';

const generationAvailable = async () => {
  const { data } = await apiClient.get<IGenerationAvailableResponse>(
    API_ENDPOINTS.GENERATION_AVAILABLE
  );
  return data;
};

const getProjectKeys = async () => {
  const { data } = await apiClient.get<IGetProjectKeysResponse>(
    API_ENDPOINTS.PROJECT_KEYS
  );
  return data;
};

const getReport = async (id: string) => {
  const { data } = await apiClient.get<IQAReport>(API_ENDPOINTS.REPORT(id));
  return data;
};

const postReport = async (report: IPostReportRequest) => {
  const { data } = await apiClient.post(API_ENDPOINTS.REPORTS, report);
  return data;
};

const generateReport = async ({
  id,
  resolutions_by_platforms,
  file
}: IGenerateReportRequest) => {
  const reportData = new FormData();
  if (file) reportData.append('file', file);
  reportData.append('resolutions_by_platforms', resolutions_by_platforms);

  const { data } = await apiClient.post(
    API_ENDPOINTS.GENERATE_REPORT(id),
    reportData,
    {
      headers: {
        'Content-Type': `multipart/form-data`
      }
    }
  );
  return data;
};

const downloadReport = async (url: string) => {
  const { data } = await axios.get(url, {
    responseType: 'blob'
  });
  return data;
};

const removeReport = async (id: string) =>
  apiClient.delete(API_ENDPOINTS.REPORT(id));

export default {
  generationAvailable,
  getProjectKeys,
  getReport,
  postReport,
  generateReport,
  downloadReport,
  removeReport
};
