import type { IQAReport } from '../report/types';

export interface IGetReportsRequest {
  page_size: number;
  page?: number;
  ordering?: string;
  project_key: string;
  jira_link: string;
}

export interface IGetReportsResponse {
  count?: number;
  next: string | null;
  previous: string | null;
  results: IQAReport[];
}
