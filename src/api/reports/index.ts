import { IGetReportsRequest, IGetReportsResponse } from './types';
import apiClient from '../index';

import API_ENDPOINTS from '../endpoints';

const getReports = async (params: IGetReportsRequest) => {
  const { data } = await apiClient.get<IGetReportsResponse>(
    API_ENDPOINTS.REPORTS,
    {
      params
    }
  );
  return data;
};

export default {
  getReports
};
