import {
  IGetProjectsRequest,
  IGetProjectsResponse,
  IProject,
  IDefectRemovalTrend
} from './types';
import apiClient from '../index';

import API_ENDPOINTS from '../endpoints';

const getProject = async (id: string) => {
  const { data } = await apiClient.get<IProject>(API_ENDPOINTS.PROJECT(id));
  return data;
};

const getProjects = async (params: IGetProjectsRequest) => {
  const { data } = await apiClient.get<IGetProjectsResponse>(
    API_ENDPOINTS.PROJECTS,
    {
      params
    }
  );
  return data;
};

const getDRT = async (id: string) => {
  const { data } = await apiClient.get<IDefectRemovalTrend>(
    API_ENDPOINTS.DRT(id)
  );
  return data;
};

export default {
  getProject,
  getProjects,
  getDRT
};
