export interface IProject {
  id: string;
  project_key: string;
  created_at: string;
  updated_at: string;
  jira_link: string;
  open_bugs: {
    High: string;
    Low: string;
    Medium: string;
    Quality: string;
  };
}

export interface IGetProjectsRequest {
  page_size: number;
  page?: number;
  ordering?: string;
}

export interface IGetProjectsResponse {
  count?: number;
  next: string | null;
  previous: string | null;
  results: IProject[];
}

export type IDefectRemovalTrend = {
  total: number;
  status: number | string;
  sprint_number: string;
}[][];
