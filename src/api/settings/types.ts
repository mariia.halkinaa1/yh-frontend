export interface ISettings {
  id: string;
  defect_removal_trend: string | null;
  jira_bug_status: string | null;
  jira_delivered_stories_statuses: string | null;
  jira_issue_status_close: string | null;
  jira_issue_status_open: string | null;
  jira_issue_status_reopen: string | null;
  jira_issue_priority_high: string | null;
  jira_issue_priority_medium: string | null;
  jira_issue_priority_low: string | null;
  jira_link: string | null;
  jira_email: string | null;
  jira_project_key: string | null;
  jira_team_android: string | null;
  jira_team_backend: string | null;
  jira_team_frontend: string | null;
  jira_team_ios: string | null;
  jira_time_zone: string | null;
  jira_token: string | null;
}

export interface IGetSettingsResponse {
  count?: number;
  next: string | null;
  previous: string | null;
  results: ISettings[];
}

export interface IPutSettingsRequest {
  id: string;
  jira_bug_status: string;
  jira_delivered_stories_statuses: string;
  jira_issue_status_close: string;
  jira_issue_status_open: string;
  jira_issue_status_reopen: string;
  jira_issue_priority_high: string;
  jira_issue_priority_medium: string;
  jira_issue_priority_low: string;
  jira_link: string;
  jira_email: string | null;
  jira_project_key: string;
  jira_team_android: string | null;
  jira_team_backend: string | null;
  jira_team_frontend: string | null;
  jira_team_ios: string | null;
  jira_time_zone: string;
  jira_token: string;
  defect_removal_trend?: string;
}
