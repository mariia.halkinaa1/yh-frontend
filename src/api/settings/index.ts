import { IGetSettingsResponse, IPutSettingsRequest } from './types';
import apiClient from '../index';

import API_ENDPOINTS from '../endpoints';

const getSettings = async () => {
  const { data } = await apiClient.get<IGetSettingsResponse>(
    API_ENDPOINTS.SETTINGS
  );
  return data;
};

const getTimezones = async () => {
  const { data } = await apiClient.get<string[]>(
    API_ENDPOINTS.SETTINGS_TIMEZONES
  );
  return data;
};

const putSettings = async ({ id, ...settings }: IPutSettingsRequest) => {
  const { data } = await apiClient.put(
    `${API_ENDPOINTS.SETTINGS}${id}/`,
    settings
  );
  return data;
};

export default {
  getSettings,
  getTimezones,
  putSettings
};
