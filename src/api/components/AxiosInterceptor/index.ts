import { useEffect } from 'react';

import apiClient from '../../index';

const AxiosInterceptor = ({ children }: { children: JSX.Element }) => {
  useEffect(() => {
    const requestInterceptor = apiClient.interceptors.request.use((config) => config);

    const responseInterceptor = apiClient.interceptors.response.use(
      (response) => response,
      async (error) => Promise.reject(error)
    );

    return () => {
      apiClient.interceptors.request.eject(requestInterceptor);
      apiClient.interceptors.response.eject(responseInterceptor);
    };
  });
  return children;
};

export default AxiosInterceptor;
