const PATH = {
  LOGIN: '/login',
  DASHBOARD: '/',
  PROJECTS: '/projects',
  PROJECT: '/project',
  SETTINGS: '/settings',
  SPRINT_REPORT: '/sprint-report',
  DELIVERY_REPORT: '/delivery-report',
  SPRINT_REPORT_PREVIEW: '/sprint-report-preview',
  DELIVERY_REPORT_PREVIEW: '/delivery-report-preview'
};

export default PATH;
