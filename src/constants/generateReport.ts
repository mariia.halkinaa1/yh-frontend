export enum ReportType {
  SPRINT_REPORT = 'SPRINT_REPORT',
  DELIVERY_REPORT = 'DELIVERY_REPORT'
}

export enum Resolutions {
  APPROVE = 'Approve',
  NOT_RECOMMEND = 'Not recommend',
  NOT_APPROVE = 'Not approve'
}

export const resolutions = [
  {
    value: Resolutions.APPROVE,
    label: Resolutions.APPROVE
  },
  {
    value: Resolutions.NOT_RECOMMEND,
    label: Resolutions.NOT_RECOMMEND
  },
  {
    value: Resolutions.NOT_APPROVE,
    label: Resolutions.NOT_APPROVE
  }
];
