import * as colors from '@atlaskit/theme/colors';

const THEME_COLOR = {
  PRIMARY: colors.B400,
  SECONDARY: colors.N20,
  WHITE: colors.N0,
  ERROR: colors.R400,
  SUCCESS: colors.G300,
  GREEN: colors.G400,
  YELLOW: colors.Y300,
  RED: colors.R300
};

export default THEME_COLOR;
