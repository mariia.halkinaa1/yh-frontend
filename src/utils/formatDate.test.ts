import { formatDate, formatDateAndTime } from './formatDate';

const date = '2023-02-08T08:00:00';

it('formatDate', () => {
  expect(formatDate(date)).toEqual('08/02/2023');
  expect(formatDate()).toEqual(null);
  expect(formatDate(date)).not.toEqual('02/08/2023');
});

it('formatDateAndTime', () => {
  expect(formatDateAndTime(date)).toEqual('08/02/2023 8:00 AM');
  expect(formatDateAndTime()).toEqual(null);
  expect(formatDateAndTime(date)).not.toEqual('02/08/2023 11:00 AM');
});
