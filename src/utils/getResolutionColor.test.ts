import { Resolutions } from '../constants/generateReport';
import THEME_COLOR from '../constants/themeColor';
import getResolutionColor from './getResolutionColor';

it('Get resolution field color', () => {
  expect(getResolutionColor(Resolutions.APPROVE)).toEqual(THEME_COLOR.GREEN);
  expect(getResolutionColor(Resolutions.APPROVE)).not.toEqual('inherit');
  expect(getResolutionColor(Resolutions.NOT_RECOMMEND)).toEqual(
    THEME_COLOR.YELLOW
  );
  expect(getResolutionColor(Resolutions.APPROVE)).not.toEqual('inherit');
  expect(getResolutionColor(Resolutions.NOT_APPROVE)).toEqual(THEME_COLOR.RED);
  expect(getResolutionColor(Resolutions.APPROVE)).not.toEqual('inherit');
});
