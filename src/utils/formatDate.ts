import dateFormat from 'dateformat';

export const formatDate = (date?: string) => {
  return date ? dateFormat(date, 'dd/mm/yyyy') : null;
};

export const formatDateAndTime = (date?: string) => {
  return date ? dateFormat(date, 'dd/mm/yyyy h:MM TT') : null;
};
