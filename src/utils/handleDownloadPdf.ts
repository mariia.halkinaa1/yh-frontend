import reportApi from '../api/report';

const handleDownloadPdf = async (url: string) => {
  const urlParts = url.split('/');
  const fileName = urlParts[urlParts.length - 1];
  const blob = await reportApi.downloadReport(url);
  const href = URL.createObjectURL(blob);

  if (window.showSaveFilePicker) {
    const handle = await window.showSaveFilePicker({
      suggestedName: fileName,
      types: [
        {
          accept: { 'application/pdf': ['.pdf'] }
        }
      ]
    });
    const writable = await handle.createWritable();
    await writable.write(blob);

    await writable.close();
  } else {
    const link = document.createElement('a');
    link.setAttribute('href', href);
    link.setAttribute('download', fileName);
    document.body.appendChild(link);
    link.click();

    document.body.removeChild(link);
  }

  URL.revokeObjectURL(href);
};

export default handleDownloadPdf;
