import { Resolutions } from '../constants/generateReport';
import THEME_COLOR from '../constants/themeColor';

const getResolutionColor = (value: Resolutions) => {
  if (value === Resolutions.APPROVE) {
    return THEME_COLOR.GREEN;
  } else if (value === Resolutions.NOT_RECOMMEND) {
    return THEME_COLOR.YELLOW;
  } else if (value === Resolutions.NOT_APPROVE) {
    return THEME_COLOR.RED;
  }

  return 'inherit';
};

export default getResolutionColor;
