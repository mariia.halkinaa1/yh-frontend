import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';

import App from './App';
import AxiosInterceptor from './api/components/AxiosInterceptor';
import { NotificationProvider } from './context/notificationContext';

import '@atlaskit/css-reset';
import './index.scss';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnReconnect: false,
      retry: 1,
      staleTime: 5 * 1000
    }
  }
});

ReactDOM.render(
  <BrowserRouter>
    <QueryClientProvider client={queryClient}>
      <NotificationProvider>
        <AxiosInterceptor>
          <App />
        </AxiosInterceptor>
      </NotificationProvider>
    </QueryClientProvider>
  </BrowserRouter>,
  document.getElementById('root')
);
