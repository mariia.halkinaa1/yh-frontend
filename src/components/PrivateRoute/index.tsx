import React from 'react';
import { Navigate } from 'react-router-dom';

import PATH from '../../constants/routes';

interface IPrivateRoute {
  children: JSX.Element;
  isAuth: boolean;
}

const PrivateRoute = ({ children, isAuth }: IPrivateRoute) => {
  if (!isAuth) {
    return <Navigate to={{ pathname: PATH.LOGIN }} />;
  }
  return children;
};

export default PrivateRoute;
