import React from 'react';

import ButtonAK from '@atlaskit/button';
import type { ButtonProps } from '@atlaskit/button/types';

import styles from './style.module.scss';

interface IButton
  extends Pick<
    ButtonProps,
    | 'type'
    | 'children'
    | 'component'
    | 'onClick'
    | 'iconBefore'
    | 'appearance'
    | 'spacing'
    | 'isDisabled'
  > {
  isTransparent?: boolean;
  className?: string;
}

const Button = ({
  type,
  children,
  component,
  onClick,
  iconBefore,
  appearance,
  spacing,
  isDisabled,
  isTransparent = false,
  className
}: IButton) => {
  let buttonClassName;
  switch (true) {
    case className && isTransparent:
      buttonClassName = `${className} ${styles.transparent_button}`;
      break;
    case !!className:
      buttonClassName = className;
      break;
    case isTransparent:
      buttonClassName = styles.transparent_button;
      break;
    default:
      buttonClassName = '';
  }

  return (
    <ButtonAK
      type={type}
      onClick={onClick}
      iconBefore={iconBefore}
      component={component}
      appearance={appearance}
      spacing={spacing}
      isDisabled={isDisabled}
      className={buttonClassName}
    >
      {children}
    </ButtonAK>
  );
};

export default Button;
