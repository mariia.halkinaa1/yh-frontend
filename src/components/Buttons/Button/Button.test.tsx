import React from 'react';
import { render, screen } from '@testing-library/react';
import Button from './';

it('renders <Button />', () => {
  render(<Button>Button</Button>);
  expect(screen.getByRole('button')).toBeInTheDocument();
});

it('renders <Button /> with `transparent_button` class', () => {
  render(<Button isTransparent>Button</Button>);
  expect(screen.getByRole('button')).toHaveClass('transparent_button');
});

it('simulates click events', () => {
  const click = jest.fn();
  render(<Button onClick={click}>Click</Button>);

  screen.getByRole('button').click();
  expect(click).toHaveBeenCalledTimes(1);
});

it('will not call onClick when disabled', () => {
  const click = jest.fn();
  render(
    <Button onClick={click} isDisabled={true}>
      Click
    </Button>
  );

  screen.getByRole('button').click();
  expect(click).not.toHaveBeenCalled();
});
