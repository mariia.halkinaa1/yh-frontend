import React from 'react';
import { render, screen } from '@testing-library/react';
import BackButton from './';

it('renders <BackButton />', () => {
  const click = jest.fn();
  render(<BackButton onClick={click} />);
  expect(screen.getByRole('button')).toBeInTheDocument();
});

it('renders <BackButton /> with `transparent_button` class', () => {
  const click = jest.fn();
  render(<BackButton onClick={click} />);
  expect(screen.getByRole('button')).toHaveClass('transparent_button');
});

it('simulates click events', () => {
  const click = jest.fn();
  render(<BackButton onClick={click} />);

  screen.getByRole('button').click();
  expect(click).toHaveBeenCalledTimes(1);
});

it('renders <BackButton /> icon', () => {
  const click = jest.fn();
  render(<BackButton onClick={click} />);

  expect(screen.getByLabelText(/Go back button/i)).toBeInTheDocument();
});
