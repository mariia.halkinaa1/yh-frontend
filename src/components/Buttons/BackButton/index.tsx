import React from 'react';
import Button from '../Button';
import ChevronLeftLargeIcon from '@atlaskit/icon/glyph/chevron-left-large';
import type { ButtonProps } from '@atlaskit/button/types';

import styles from './style.module.scss';

const BackButton = ({ onClick }: { onClick: ButtonProps['onClick'] }) => {
  return (
    <Button
      isTransparent
      spacing="none"
      appearance="link"
      className={styles.back_button}
      iconBefore={<ChevronLeftLargeIcon label="Go back button" size="large" />}
      onClick={onClick}
    >
      Back
    </Button>
  );
};

export default BackButton;
