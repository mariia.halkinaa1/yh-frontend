import React from 'react';

import PdfDocument16Icon from '@atlaskit/icon-file-type/glyph/pdf-document/16';
import Button from '../Button';

const SavePdfButton = ({ onSaveReport }: { onSaveReport: () => void }) => {
  return (
    <Button isTransparent onClick={onSaveReport}>
      <PdfDocument16Icon label="saveReportPDF" />
    </Button>
  );
};

export default SavePdfButton;
