import React from 'react';
import { render, screen } from '@testing-library/react';
import SavePdfButton from './';

it('renders <SavePdfButton />', () => {
  const click = jest.fn();
  render(<SavePdfButton onSaveReport={click} />);
  expect(screen.getByRole('button')).toBeInTheDocument();
});

it('renders PDF icon', () => {
  const click = jest.fn();
  render(<SavePdfButton onSaveReport={click} />);
  expect(screen.getByLabelText(/saveReportPDF/i)).toBeInTheDocument();
});

it('renders <SavePdfButton /> with defined class', () => {
  const click = jest.fn();
  render(<SavePdfButton onSaveReport={click} />);
  expect(screen.getByRole('button')).toHaveClass('transparent_button');
});

it('simulates click event on Button', () => {
  const click = jest.fn();
  render(<SavePdfButton onSaveReport={click} />);

  screen.getByRole('button').click();
  expect(click).toHaveBeenCalledTimes(1);
});
