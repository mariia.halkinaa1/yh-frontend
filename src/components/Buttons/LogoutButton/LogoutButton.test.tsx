import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { render, screen } from '@testing-library/react';
import LogoutButton from './';

const Button = () => (
  <GoogleOAuthProvider clientId={''}>
    <BrowserRouter>
      <LogoutButton />
    </BrowserRouter>
  </GoogleOAuthProvider>
);

it('renders <LogoutButton />', () => {
  render(<Button />);
  expect(screen.getByRole('button')).toBeInTheDocument();
});

it('simulates click events', () => {
  render(<Button />);

  const button = screen.getByRole('button');
  const click = jest.spyOn(button, 'click');
  button.click();
  expect(click).toHaveBeenCalledTimes(1);
});
