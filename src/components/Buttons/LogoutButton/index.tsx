import React from 'react';
import { googleLogout } from '@react-oauth/google';
import SignOutIcon from '@atlaskit/icon/glyph/sign-out';

import Button from '../Button';
import THEME_COLOR from '../../../constants/themeColor';
import { useAuth } from '../../../hooks';

const LogoutButton = () => {
  const { logout } = useAuth();

  const onLogout = async () => {
    googleLogout();
    logout();
  };

  return (
    <Button
      onClick={onLogout}
      iconBefore={
        <SignOutIcon label="Logout" primaryColor={THEME_COLOR.WHITE} />
      }
    />
  );
};

export default LogoutButton;
