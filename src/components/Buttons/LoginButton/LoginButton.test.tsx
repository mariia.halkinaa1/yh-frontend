import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { render, screen } from '@testing-library/react';
import LoginButton from './';

const Button = () => (
  <GoogleOAuthProvider clientId={''}>
    <BrowserRouter>
      <LoginButton />
    </BrowserRouter>
  </GoogleOAuthProvider>
);

it('renders <LoginButton />', () => {
  render(<Button />);
  expect(screen.getByTestId('loginButton')).toBeInTheDocument();
});

it('simulates click events', () => {
  render(<Button />);

  const button = screen.getByTestId('loginButton');
  const click = jest.spyOn(button, 'click');
  button.click();
  expect(click).toHaveBeenCalledTimes(1);
});
