import React, { useContext } from 'react';
import { GoogleLogin } from '@react-oauth/google';
import jwt_decode from 'jwt-decode';

import type { CredentialResponse } from '@react-oauth/google';

import NotificationContext from '../../../context/notificationContext';
import { useAuth, useUserLocalStorage } from '../../../hooks';
import errorMessages from '../../../helpers/messages/errorMsg';

interface ICredential {
  aud: string;
  azp: string;
  email: string;
  email_verified: boolean;
  exp: number;
  family_name: string;
  given_name: string;
  iss: string;
  jti: string;
  name: string;
  nbf: number;
  picture: string;
  sub: string;
}

const LoginButton = () => {
  const { onError } = useContext(NotificationContext);
  const { login } = useAuth();
  const { setUser } = useUserLocalStorage();

  const onSuccess: (credentialResponse: CredentialResponse) => void = async ({
    credential
  }) => {
    try {
      const user: ICredential = jwt_decode(credential as string);

      setUser(JSON.stringify(user));
      await login(credential as string);
    } catch (error) {
      onError(errorMessages.ErrorGoogleLogin);
    }
  };

  return (
    <div data-testid="loginButton">
      <GoogleLogin onSuccess={onSuccess} />
    </div>
  );
};

export default LoginButton;
