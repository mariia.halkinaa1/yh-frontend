import React from 'react';
import { render, screen } from '@testing-library/react';
import LoadingButton from './';

it('renders <LoadingButton />', () => {
  render(<LoadingButton />);
  expect(screen.getByRole('button')).toBeInTheDocument();
});
