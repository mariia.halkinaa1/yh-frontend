import React from 'react';

import { LoadingButton as LoadingButtonAK } from '@atlaskit/button';
import type { ButtonProps } from '@atlaskit/button/types';

type IButton = Pick<
  ButtonProps,
  'type' | 'children' | 'onClick' | 'iconBefore' | 'appearance' | 'isDisabled'
>;

const LoadingButton = ({
  type,
  children,
  onClick,
  iconBefore,
  appearance,
  isDisabled
}: IButton) => {
  return (
    <LoadingButtonAK
      type={type}
      onClick={onClick}
      iconBefore={iconBefore}
      appearance={appearance}
      isDisabled={isDisabled}
    >
      {children}
    </LoadingButtonAK>
  );
};

export default LoadingButton;
