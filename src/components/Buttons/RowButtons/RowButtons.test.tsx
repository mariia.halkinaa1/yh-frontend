import React from 'react';
import { render, screen } from '@testing-library/react';
import RowButtons from './';

it('renders <RowButtons />', () => {
  render(<RowButtons />);
  expect(screen.getByRole('button')).toBeInTheDocument();
});

it('renders only Add Button by default', () => {
  render(<RowButtons />);
  expect(screen.getAllByRole('button')).toHaveLength(1);
});

it('renders Add Button and Remove button', () => {
  render(<RowButtons showRemoveButton />);
  expect(screen.getAllByRole('button')).toHaveLength(2);
});

it('does not render any button', () => {
  render(<RowButtons showAddButton={false} />);
  expect(screen.queryAllByRole('button')).toHaveLength(0);
});

it('renders Add and Remove icons', () => {
  render(<RowButtons showRemoveButton />);
  expect(screen.getByLabelText(/add-row/i)).toBeInTheDocument();
  expect(screen.getByLabelText(/remove-row/i)).toBeInTheDocument();
});

it('simulates click event on Add Button', () => {
  const handleAddRow = jest.fn();
  render(<RowButtons handleAddRow={handleAddRow} />);

  const button = screen.getByRole('button');
  button.click();
  expect(handleAddRow).toHaveBeenCalledTimes(1);
});

it('simulates click event on Remove Button', () => {
  const handleRemoveRow = jest.fn();
  render(<RowButtons handleRemoveRow={handleRemoveRow} showRemoveButton />);

  const button = screen.queryAllByRole('button')[1];
  button.click();
  expect(handleRemoveRow).toHaveBeenCalledTimes(1);
});

it('does not simulate click event on Remove Button, if there is not any', () => {
  const handleAddRow = jest.fn();
  const handleRemoveRow = jest.fn();
  render(
    <RowButtons handleAddRow={handleAddRow} handleRemoveRow={handleRemoveRow} />
  );

  const button = screen.getByRole('button');
  button.click();
  expect(handleAddRow).toHaveBeenCalledTimes(1);
  expect(handleRemoveRow).not.toHaveBeenCalled();
});
