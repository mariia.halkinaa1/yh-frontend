import React from 'react';
import AddIcon from '@atlaskit/icon/glyph/add';
import CrossIcon from '@atlaskit/icon/glyph/cross';

import Button from '../Button';

const RowButtons = ({
  showAddButton = true,
  showRemoveButton = false,
  handleAddRow,
  handleRemoveRow
}: {
  showAddButton?: boolean;
  showRemoveButton?: boolean;
  handleAddRow?: () => void;
  handleRemoveRow?: () => void;
}) => {
  return (
    <>
      {showAddButton ? (
        <Button
          onClick={handleAddRow}
          isTransparent
          iconBefore={<AddIcon label="add-row" size="medium" />}
        />
      ) : null}
      {showRemoveButton ? (
        <Button
          onClick={handleRemoveRow}
          isTransparent
          iconBefore={<CrossIcon label="remove-row" size="medium" />}
        />
      ) : null}
    </>
  );
};

export default RowButtons;
