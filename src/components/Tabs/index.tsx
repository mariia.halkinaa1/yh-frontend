import React, { useMemo, useCallback, ReactNode } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import TabsAK, { TabList, Tab, TabPanel } from '@atlaskit/tabs';

import styles from './style.module.scss';

function Tabs({
  id,
  tabs,
  defaultPath
}: {
  id: string;
  tabs: { label: string; url: string; content: ReactNode }[];
  defaultPath: string;
}) {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const defaultSelected = useMemo(
    () => tabs.findIndex((tab) => tab.url === defaultPath),
    [tabs, defaultPath]
  );

  const selected = useMemo(
    () => tabs.findIndex((tab) => tab.url === pathname),
    [pathname, tabs]
  );

  const handleUpdate = useCallback(
    (tabIndex: number) => {
      navigate(tabs[tabIndex].url, { replace: true });
    },
    [navigate, tabs]
  );

  return (
    <div className={styles.tabs_container}>
      <TabsAK
        id={id}
        onChange={handleUpdate}
        defaultSelected={defaultSelected}
        selected={selected}
      >
        <TabList>
          {tabs.map(({ label }) => (
            <Tab key={label}>{label}</Tab>
          ))}
        </TabList>
        {tabs.map(({ label, content }) => (
          <TabPanel key={label}>{content}</TabPanel>
        ))}
      </TabsAK>
    </div>
  );
}

export default Tabs;
