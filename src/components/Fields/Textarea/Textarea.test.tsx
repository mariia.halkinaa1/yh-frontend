import React from 'react';
import { render } from '@testing-library/react';

import Textarea from './';

it('renders <Textarea />', () => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <Textarea
      value=""
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container.querySelector('label')).toBeInTheDocument();
  expect(container).toHaveTextContent('Test');
  expect(container.querySelector('textarea')).toHaveValue('');
  expect(container.querySelector('textarea')).toHaveAttribute(
    'placeholder',
    'Some placeholder'
  );
});

it('renders <Textarea /> with value', () => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <Textarea
      value="Some value"
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container.querySelector('textarea')).toHaveValue('Some value');
  expect(container).not.toHaveTextContent('Some placeholder');
});
