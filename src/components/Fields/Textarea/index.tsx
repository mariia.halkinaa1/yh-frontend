import React from 'react';
import Field from '@atlaskit/form/Field';
import { ErrorMessage, HelperMessage } from '@atlaskit/form';
import TextArea from '@atlaskit/textarea';

import type { FieldInputProps } from 'formik';

const Textarea = ({
  name,
  label,
  placeholder,
  value,
  onChange,
  onBlur,
  errorMsg,
  helperMsg
}: {
  name: string;
  label: string;
  value: string;
  placeholder?: string;
  onChange: FieldInputProps<HTMLInputElement>['onChange'];
  onBlur?: FieldInputProps<HTMLInputElement>['onBlur'];
  errorMsg?: JSX.Element | string;
  helperMsg?: JSX.Element | string;
}) => {
  return (
    <Field name={name} label={label} defaultValue={value}>
      {() => (
        <>
          <TextArea
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={onChange}
            onBlur={onBlur}
          />
          {helperMsg && <HelperMessage>{helperMsg}</HelperMessage>}
          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
        </>
      )}
    </Field>
  );
};

export default Textarea;
