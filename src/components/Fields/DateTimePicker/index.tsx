import React, { useMemo } from 'react';
import Field from '@atlaskit/form/Field';
import { ErrorMessage, HelperMessage } from '@atlaskit/form';
import { DateTimePicker as DateTimePickerAK } from '@atlaskit/datetime-picker';

import { formatDate } from '../../../utils/formatDate';

const DateTimePicker = ({
  name,
  label,
  value,
  onChange,
  onBlur,
  isRequired = false,
  isDisabled = false,
  errorMsg,
  helperMsg,
  minDate
}: {
  name: string;
  label: string;
  value: string;
  onChange: (value: string) => void;
  onBlur?: () => void;
  isRequired?: boolean;
  isDisabled?: boolean;
  errorMsg?: JSX.Element | string;
  helperMsg?: JSX.Element | string;
  minDate?: string;
}) => {
  const initialDate = useMemo(() => new Date().toISOString(), []);

  const handleOnChangeDate = (date: string) => {
    onChange(`${date} 09:00`);
  };

  return (
    <Field
      name={name}
      label={label}
      isRequired={isRequired}
      defaultValue={value}
    >
      {() => (
        <>
          <DateTimePickerAK
            name={name}
            value={value}
            isInvalid={!!errorMsg}
            onChange={onChange}
            onBlur={onBlur}
            timeIsEditable
            isDisabled={isDisabled}
            datePickerProps={{
              minDate,
              placeholder: formatDate(initialDate),
              onChange: handleOnChangeDate
            }}
            timePickerProps={{
              placeholder: '9:00 AM'
            }}
            dateFormat="DD/MM/YYYY"
          />
          {helperMsg && <HelperMessage>{helperMsg}</HelperMessage>}
          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
        </>
      )}
    </Field>
  );
};

export default DateTimePicker;
