import React from 'react';
import { render } from '@testing-library/react';

import { formatDate } from '../../../utils/formatDate';
import DateTimePicker from './';

const setup = (initialValue = '') => {
  const labelValue = 'Sprint Start Date';
  const time = '9:00 AM';
  const onChangeFn = jest.fn();
  const today = new Date().toISOString();

  const { container } = render(
    <DateTimePicker
      value={initialValue}
      label={labelValue}
      name="Sprint Start Date"
      onChange={onChangeFn}
    />
  );

  return {
    container,
    today,
    labelValue,
    time
  };
};

it('renders <DateTimePicker /> with placeholders', () => {
  const { container, today, labelValue, time } = setup();

  expect(container.querySelector('label')).toHaveTextContent(labelValue);
  expect(container).toHaveTextContent(labelValue);
  expect(container).toHaveTextContent(formatDate(today));
  expect(container).toHaveTextContent(time);
});

it('renders <DateTimePicker /> with initial values', () => {
  const date = '2022-12-25T09:00:00';
  const { container, today, time } = setup(date);

  expect(container).toHaveTextContent(formatDate(date));
  expect(container).not.toHaveTextContent(formatDate(today));
  expect(container).toHaveTextContent(time);
});
