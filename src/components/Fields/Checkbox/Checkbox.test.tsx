import React, { useState } from 'react';
import { render, fireEvent } from '@testing-library/react';
import Checkbox from './';

const CheckboxComponent = ({
  isChecked,
  onChange,
  isDisabled = false
}: {
  isChecked: boolean;
  onChange: () => void;
  isDisabled?: boolean;
}) => {
  const [checked, setChecked] = useState(isChecked);

  const handleOnChange = () => {
    if (!isDisabled) {
      setChecked(!checked);
      onChange();
    }
  };

  return (
    <Checkbox
      onChange={handleOnChange}
      label="Checkbox label"
      name="Checkbox"
      isChecked={checked}
      isDisabled={isDisabled}
    />
  );
};

const setup = (isDisabled = false) => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <CheckboxComponent
      onChange={onChangeFn}
      isChecked={false}
      isDisabled={isDisabled}
    />
  );

  return {
    input: container.querySelector('input'),
    label: container.querySelector('label'),
    onChangeFn
  };
};

it('renders <Checkbox />', () => {
  const { label, input } = setup();

  expect(label).toBeInTheDocument();
  expect(label).toHaveTextContent('Checkbox label');

  expect(input).toBeInTheDocument();
  expect(input).not.toBeChecked();
});

it('calls onChange handler by clicking on Checkbox', () => {
  const { input, onChangeFn } = setup();

  fireEvent.click(input as HTMLInputElement);

  expect(onChangeFn).toHaveBeenCalledTimes(1);
});

it('changes Checkbox value by onChange handler', () => {
  const { input } = setup();

  expect(input).not.toBeChecked();

  fireEvent.click(input as HTMLInputElement);

  expect(input).toBeChecked();
});

it('does not change Checkbox value by clicking on disabled Checkbox', () => {
  const { input, onChangeFn } = setup(true);

  expect(input).not.toBeChecked();
  expect(input).toHaveAttribute('disabled');

  fireEvent.click(input as HTMLInputElement);

  expect(input).not.toBeChecked();
  expect(onChangeFn).toHaveBeenCalledTimes(0);
});
