import React from 'react';
import Field from '@atlaskit/form/Field';
import { Checkbox as CheckboxAK } from '@atlaskit/checkbox';

import type { FieldInputProps } from 'formik';

const Checkbox = ({
  name,
  label,
  isChecked,
  onChange,
  onBlur,
  isDisabled = false
}: {
  name: string;
  label: string;
  isChecked: boolean;
  onChange: FieldInputProps<HTMLInputElement>['onChange'];
  onBlur?: FieldInputProps<HTMLInputElement>['onBlur'];
  isDisabled?: boolean;
}) => {
  return (
    <Field name={name} label={label} defaultValue={isChecked}>
      {() => (
        <CheckboxAK
          name={name}
          isChecked={isChecked}
          onChange={onChange}
          onBlur={onBlur}
          isDisabled={isDisabled}
        />
      )}
    </Field>
  );
};

export default Checkbox;
