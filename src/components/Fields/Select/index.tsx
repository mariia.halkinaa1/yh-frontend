import React from 'react';
import Field from '@atlaskit/form/Field';
import Select, { StylesConfig } from '@atlaskit/select';
import { ErrorMessage } from '@atlaskit/form';

type TSelectOption = {
  label: string;
  value: string;
};

const SelectInput = ({
  options,
  name,
  label,
  value,
  onChange,
  onBlur,
  placeholder,
  isRequired = false,
  isDisabled = false,
  isLoading = false,
  errorMsg,
  styles
}: {
  options: TSelectOption[];
  name: string;
  label: string;
  value: TSelectOption;
  onChange?: (option: TSelectOption) => void;
  onBlur?: () => void;
  placeholder?: string;
  isRequired?: boolean;
  isDisabled?: boolean;
  isLoading?: boolean;
  errorMsg?: JSX.Element | string;
  styles?: StylesConfig;
}) => {
  return (
    <Field name={name} label={label} isRequired={isRequired}>
      {() => (
        <>
          <Select
            defaultValue={!!value.value ? value : undefined}
            value={!!value.value ? value : undefined}
            name={name}
            options={options}
            placeholder={placeholder}
            onChange={onChange}
            onBlur={onBlur}
            isDisabled={isDisabled}
            isLoading={isLoading}
            isInvalid={!!errorMsg}
            styles={styles}
          />
          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
        </>
      )}
    </Field>
  );
};

export default SelectInput;
