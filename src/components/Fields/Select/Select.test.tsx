import React from 'react';
import { render } from '@testing-library/react';

import Select from './';

const onChangeFn = jest.fn();
const options = [
  { value: 'green', label: 'Green' },
  { value: 'red', label: 'Red' }
];

it('renders <Select />', () => {
  const { container } = render(
    <Select
      options={options}
      value={{ value: '', label: '' }}
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container.querySelector('label')).toBeInTheDocument();
  expect(container).toHaveTextContent('Test');
  expect(container.querySelectorAll('input')[1]).toHaveValue('');
  expect(container).not.toHaveTextContent('Green');
  expect(container).not.toHaveTextContent('Red');
  expect(container).toHaveTextContent('Some placeholder');
});

it('renders <Select /> with value', () => {
  const { container } = render(
    <Select
      options={options}
      value={options[0]}
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container).toHaveTextContent('Green');
  expect(container).not.toHaveTextContent('Red');
  expect(container.querySelectorAll('input')[1]).toHaveValue('green');
  expect(container).not.toHaveTextContent('Some placeholder');
});
