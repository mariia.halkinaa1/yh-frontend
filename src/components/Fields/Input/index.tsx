import React from 'react';
import Field from '@atlaskit/form/Field';
import { ErrorMessage, HelperMessage } from '@atlaskit/form';
import Textfield from '@atlaskit/textfield';

import type { FieldInputProps } from 'formik';

const Input = ({
  name,
  label,
  placeholder,
  value,
  onChange,
  onBlur,
  isRequired = false,
  errorMsg,
  helperMsg
}: {
  name: string;
  label: string;
  value: string;
  onChange: FieldInputProps<HTMLInputElement>['onChange'];
  onBlur?: FieldInputProps<HTMLInputElement>['onBlur'];
  placeholder?: string;
  isRequired?: boolean;
  errorMsg?: JSX.Element | string;
  helperMsg?: JSX.Element | string;
}) => {
  return (
    <Field
      name={name}
      label={label}
      isRequired={isRequired}
      defaultValue={value}
    >
      {() => (
        <>
          <Textfield
            name={name}
            value={value}
            placeholder={placeholder}
            isInvalid={!!errorMsg}
            onChange={onChange}
            onBlur={onBlur}
          />
          {helperMsg && <HelperMessage>{helperMsg}</HelperMessage>}
          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
        </>
      )}
    </Field>
  );
};

export default Input;
