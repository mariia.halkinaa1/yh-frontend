import React from 'react';
import { render } from '@testing-library/react';

import Input from './';

it('renders <Input />', () => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <Input
      value=""
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container.querySelector('label')).toBeInTheDocument();
  expect(container).toHaveTextContent('Test');
  expect(container.querySelector('input')).toHaveValue('');
  expect(container.querySelector('input')).toHaveAttribute(
    'placeholder',
    'Some placeholder'
  );
});

it('renders <Input /> with value', () => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <Input
      value="Some value"
      label="Test"
      name="test"
      onChange={onChangeFn}
      placeholder="Some placeholder"
    />
  );

  expect(container.querySelector('input')).toHaveValue('Some value');
  expect(container).not.toHaveTextContent('Some placeholder');
});
