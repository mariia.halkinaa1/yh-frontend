import React, { useState } from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Counter from './';

type Type = 'add' | 'subtract';

const CounterComponent = ({
  onChange,
  type,
  initialValue = '10'
}: {
  onChange: () => void;
  type?: Type;
  initialValue?: string;
}) => {
  const [quantity, setQuantity] = useState(initialValue);

  const handleChange = (value: string) => {
    if (type === 'add') setQuantity((q) => String(Number(q) + 1));
    else if (type === 'subtract') setQuantity((q) => String(Number(q) - 1));
    else setQuantity(value);
    onChange();
  };

  return (
    <Counter
      label="Counter label"
      name="Counter"
      value={quantity}
      onChange={handleChange}
    />
  );
};

const setup = ({
  type,
  initialValue
}: {
  type?: Type;
  initialValue?: string;
}) => {
  const onChangeFn = jest.fn();
  const { container } = render(
    <CounterComponent
      onChange={onChangeFn}
      type={type}
      initialValue={initialValue}
    />
  );

  return {
    input: container.querySelector('input'),
    label: container.querySelector('label'),
    onChangeFn
  };
};

it('renders <Counter />', () => {
  const { input, label } = setup({ type: 'add' });

  expect(label).toBeInTheDocument();
  expect(label).toHaveTextContent('Counter label');

  expect(input).toBeInTheDocument();
  expect(input).toHaveValue('10');

  expect(screen.getByLabelText(/counter-subtract/i)).toBeInTheDocument();
  expect(screen.getByLabelText(/counter-add/i)).toBeInTheDocument();
});

it('calls onChange handler by clicking on Add/Subtract buttons', () => {
  const { onChangeFn } = setup({ type: 'add' });

  const [subtractButton, addButton] = screen.queryAllByRole('button');

  subtractButton.click();
  addButton.click();

  expect(onChangeFn).toHaveBeenCalledTimes(2);
});

it('increases input value by clicking on Add button', () => {
  const { input } = setup({ type: 'add' });

  const [, addButton] = screen.queryAllByRole('button');

  expect(input).toHaveValue('10');

  addButton.click();
  addButton.click();

  expect(input).toHaveValue('12');
});

it('decreases input value by clicking on Subtract button', () => {
  const { input } = setup({ type: 'subtract' });

  const [subtractButton] = screen.queryAllByRole('button');

  expect(input).toHaveValue('10');

  subtractButton.click();
  subtractButton.click();
  subtractButton.click();

  expect(input).toHaveValue('7');
});

it('does not change input value when min value is reached', () => {
  const { input } = setup({ type: 'subtract', initialValue: '2' });

  const [subtractButton] = screen.queryAllByRole('button');

  expect(input).toHaveValue('2');

  subtractButton.click();
  subtractButton.click();
  subtractButton.click();

  expect(input).toHaveValue('0');
});

it('does not change input value when max value is reached', () => {
  const { input } = setup({ type: 'add', initialValue: '997' });

  const [, addButton] = screen.queryAllByRole('button');

  expect(input).toHaveValue('997');

  addButton.click();
  addButton.click();
  addButton.click();
  expect(input).toHaveValue('999');
});

it('changes input value by onChange handler', () => {
  const { input } = setup({});

  expect(input).toHaveValue('10');
  fireEvent.change(input as HTMLInputElement, { target: { value: '23' } });

  expect(input).toHaveValue('23');
});

it('does not change input value when entering non-numbers', () => {
  const { input } = setup({});

  expect(input).toHaveValue('10');
  fireEvent.change(input as HTMLInputElement, { target: { value: 'Test/.' } });

  expect(input).toHaveValue('10');
});
