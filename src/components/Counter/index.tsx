import React, { useMemo, ChangeEvent } from 'react';
import Field from '@atlaskit/form/Field';
import Textfield from '@atlaskit/textfield';
import { ErrorMessage } from '@atlaskit/form';
import AddIcon from '@atlaskit/icon/glyph/add';
import EditorDividerIcon from '@atlaskit/icon/glyph/editor/divider';

import { Button } from '../Buttons';
import THEME_COLOR from '../../constants/themeColor';

import styles from './style.module.scss';

function Counter({
  value,
  name,
  label,
  min = 0,
  max = 999,
  onChange,
  onBlur,
  isRequired = false,
  errorMsg
}: {
  value: string;
  name: string;
  label: string;
  min?: number;
  max?: number;
  onChange: (value: string) => void;
  onBlur?: () => void;
  isRequired?: boolean;
  errorMsg?: JSX.Element | string;
}) {
  const handleSubtractQuantity = () => {
    onChange(String(parseInt(value) - 1));
  };

  const handleAddQuantity = () => {
    onChange(String(parseInt(value) + 1));
  };

  const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.value === '') {
      onChange(e.target.value);
      return;
    }
    const quantityValue = parseFloat(e.target.value);
    if (!quantityValue || quantityValue < min || quantityValue > max) {
      return;
    }
    onChange(String(quantityValue));
  };

  const isDisabledSubtractButton = useMemo(
    () => !value || parseInt(value) === min,
    [value, min]
  );
  const isDisabledAddButton = useMemo(
    () => !value || parseInt(value) === max,
    [value, max]
  );

  return (
    <Field
      name={name}
      label={label}
      defaultValue={value}
      isRequired={isRequired}
    >
      {() => (
        <>
          <div className={styles.counter}>
            <Button
              onClick={handleSubtractQuantity}
              isDisabled={isDisabledSubtractButton}
              isTransparent
              iconBefore={
                <EditorDividerIcon
                  label="counter-subtract"
                  primaryColor={
                    isDisabledSubtractButton ? THEME_COLOR.SECONDARY : 'inherit'
                  }
                />
              }
            />
            <Textfield
              value={value}
              isInvalid={!!errorMsg}
              onChange={handleOnChange}
              onBlur={onBlur}
              className={styles.counter__input}
            />
            <Button
              onClick={handleAddQuantity}
              isDisabled={isDisabledAddButton}
              isTransparent
              iconBefore={
                <AddIcon
                  label="counter-add"
                  primaryColor={
                    isDisabledAddButton ? THEME_COLOR.SECONDARY : 'inherit'
                  }
                />
              }
            />
          </div>
          {errorMsg && <ErrorMessage>{errorMsg}</ErrorMessage>}
        </>
      )}
    </Field>
  );
}

export default Counter;
