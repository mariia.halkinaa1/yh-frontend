import React, { useMemo, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormik, getIn, FormikProvider, FieldArray } from 'formik';

import { Button, RowButtons } from '../../Buttons';
import Counter from '../../Counter';
import Spinner from '../../Spinner';
import { DateTimePicker, Input, Select, Textarea } from '../../Fields';
import { ReportType } from '../../../constants/generateReport';
import PATH from '../../../constants/routes';
import { useCreateReport } from '../../../hooks';
import { deliveryReportFields } from '../../../helpers/fields/qaReport';
import { deliveryReportValidationSchema } from '../../../validation/qaReport';
import { SPRINT_MIN, SPRINT_MAX, platformInitValues } from '../constants';
import {
  getDateYearAndDayAgo,
  getDateYearAgo,
  getResolutionsByPlatforms
} from '../helpers';

import styles from './style.module.scss';

function DeliveryReportForm({
  projectKey,
  projectName,
  projectKeysOptions
}: {
  projectKey: string;
  projectName: string;
  projectKeysOptions: { value: string; label: string }[];
}) {
  const navigate = useNavigate();
  const { id: projectId } = useParams();
  const { isCreatingReport, createReport } = useCreateReport();

  const [minDate, setMinDate] = useState(getDateYearAndDayAgo(new Date()));

  const initialDate = useMemo(() => new Date().toISOString(), []);

  const form = useFormik({
    initialValues: {
      sprint: '1',
      start: '',
      end: '',
      deliveryDate: initialDate,
      platforms: [platformInitValues],
      releaseNotes: ''
    },
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: deliveryReportValidationSchema,
    onSubmit: async ({
      sprint,
      start,
      end,
      deliveryDate,
      platforms,
      releaseNotes
    }) => {
      const resolutionsByPlatforms = getResolutionsByPlatforms(platforms);

      createReport(
        {
          report_type: ReportType.DELIVERY_REPORT,
          project_key: projectKey,
          sprint_number: sprint,
          sprint_start_date: start,
          sprint_end_date: end,
          delivery_date: deliveryDate,
          resolutions_by_platforms: resolutionsByPlatforms,
          release_notes: releaseNotes
        },
        {
          onSuccess: ({ id }) =>
            navigate(
              `${PATH.PROJECT}/${projectId}${PATH.DELIVERY_REPORT_PREVIEW}/${id}`,
              {
                state: {
                  projectName
                }
              }
            )
        }
      );
    }
  });

  return isCreatingReport ? (
    <Spinner />
  ) : (
    <div className={styles.delivery_report}>
      <form
        className={styles.delivery_report__form}
        onSubmit={form.handleSubmit}
      >
        <div className={styles.delivery_report__form__item}>
          <Select
            {...deliveryReportFields.projectKey}
            value={projectKeysOptions[0]}
            options={projectKeysOptions}
            isDisabled={true}
            isLoading={!projectName || !projectKey}
          />
          <Counter
            {...deliveryReportFields.sprint}
            value={form.values.sprint}
            min={SPRINT_MIN}
            max={SPRINT_MAX}
            onChange={(sprint: string) =>
              form.setFieldValue(deliveryReportFields.sprint.name, sprint)
            }
            onBlur={() =>
              form.setFieldTouched(deliveryReportFields.sprint.name)
            }
            errorMsg={
              form.touched.sprint && form.errors.sprint
                ? form.errors.sprint
                : ''
            }
          />
        </div>
        <div className={styles.delivery_report__form__item}>
          <DateTimePicker
            {...deliveryReportFields.start}
            value={form.values.start}
            minDate={minDate}
            onChange={(date: string) => {
              if (!date) {
                form.setFieldTouched(deliveryReportFields.deliveryDate.name);
              }
              form.setFieldValue(deliveryReportFields.start.name, date);
            }}
            onBlur={() => form.setFieldTouched(deliveryReportFields.start.name)}
            errorMsg={
              form.touched.start && form.errors.start ? form.errors.start : ''
            }
          />
          <DateTimePicker
            {...deliveryReportFields.end}
            value={form.values.end}
            onChange={(date: string) => {
              if (!date) {
                form.setFieldTouched(deliveryReportFields.deliveryDate.name);
              }
              form.setFieldValue(deliveryReportFields.end.name, date);
              const dateYearAgo = getDateYearAgo(new Date(date));
              setMinDate(getDateYearAndDayAgo(new Date(date)));
              if (new Date(dateYearAgo) > new Date(form.values.start)) {
                form.setFieldValue(
                  deliveryReportFields.start.name,
                  dateYearAgo
                );
              }
            }}
            onBlur={() => form.setFieldTouched(deliveryReportFields.end.name)}
            errorMsg={
              form.touched.end && form.errors.end ? form.errors.end : ''
            }
          />
          <DateTimePicker
            {...deliveryReportFields.deliveryDate}
            value={form.values.deliveryDate}
            minDate={minDate}
            onChange={(date: string) => {
              if (!date) {
                form.setFieldTouched(deliveryReportFields.deliveryDate.name);
              }
              form.setFieldValue(deliveryReportFields.deliveryDate.name, date);
            }}
            onBlur={() =>
              form.setFieldTouched(deliveryReportFields.deliveryDate.name)
            }
            errorMsg={
              form.touched.deliveryDate && form.errors.deliveryDate
                ? form.errors.deliveryDate
                : ''
            }
          />
        </div>
        <FormikProvider value={form}>
          <FieldArray
            name="platforms"
            render={({ name, push, remove }) => {
              return form.values.platforms.map(({ platform, build }, idx) => {
                const platformFieldName = `${name}.${idx}.platform`;
                const buildFieldName = `${name}.${idx}.build`;
                const platformFieldError = getIn(
                  form.errors,
                  platformFieldName
                );
                const platformFieldTouched = getIn(
                  form.touched,
                  platformFieldName
                );
                return (
                  <div
                    key={`${name}-${idx}`}
                    className={styles.delivery_report__form__item}
                  >
                    <Input
                      {...deliveryReportFields.platform}
                      name={platformFieldName}
                      value={platform}
                      onChange={form.handleChange}
                      onBlur={form.handleBlur}
                      errorMsg={
                        platformFieldTouched && platformFieldError
                          ? platformFieldError
                          : ''
                      }
                    />
                    <Input
                      {...deliveryReportFields.build}
                      name={buildFieldName}
                      value={build}
                      onChange={form.handleChange}
                      onBlur={form.handleBlur}
                    />
                    <div className={styles.delivery_report__form__row_buttons}>
                      <RowButtons
                        showRemoveButton={form.values.platforms.length > 1}
                        handleAddRow={() => push(platformInitValues)}
                        handleRemoveRow={() => remove(idx)}
                      />
                    </div>
                  </div>
                );
              });
            }}
          />
        </FormikProvider>
        <div className={styles.delivery_report__form__item}>
          <div className={styles.delivery_report__form__item__large}>
            <Textarea
              {...deliveryReportFields.releaseNotes}
              value={form.values.releaseNotes}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              errorMsg={
                form.touched.releaseNotes && form.errors.releaseNotes
                  ? form.errors.releaseNotes
                  : ''
              }
            />
          </div>
        </div>
        <Button
          type="submit"
          appearance="primary"
          isDisabled={!form.isValid || !form.dirty}
        >
          Generate Report
        </Button>
      </form>
    </div>
  );
}

export default DeliveryReportForm;
