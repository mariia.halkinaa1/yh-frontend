export const platformInitValues = {
  platform: '',
  build: ''
};

export const testRailInitValues = {
  testRun: '',
  testRailUrl: ''
};

export const SPRINT_MIN = 1;
export const SPRINT_MAX = 999;

export const DEFECT_REMOVAL_TREND_MIN = 1;
