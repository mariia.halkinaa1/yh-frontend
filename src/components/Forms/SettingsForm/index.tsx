import React, { useState, useContext, useMemo } from 'react';
import axios from 'axios';
import { useFormik } from 'formik';

import { Button } from '../../Buttons';
import Counter from '../../Counter';
import { Input, Select } from '../../Fields';
import Spinner from '../../Spinner';
import { useSettings, useTimezones, useSettingsUpdate } from '../../../hooks';
import THEME_COLOR from '../../../constants/themeColor';
import NotificationContext from '../../../context/notificationContext';
import settingsFields from '../../../helpers/fields/settings';
import settingsValidationSchema from '../../../validation/settings';
import { DEFECT_REMOVAL_TREND_MIN } from '../constants';

import styles from './style.module.scss';

function SettingsForm() {
  const { onError } = useContext(NotificationContext);
  const { isLoading, settings } = useSettings();
  const { mutate } = useSettingsUpdate();
  const { isLoadingTimezones, timezones } = useTimezones();
  const [jiraGroupsError, setJiraGroupsError] = useState('');

  const timezonesOptions = useMemo(
    () =>
      timezones?.map((timezone) => ({
        label: timezone,
        value: timezone
      })) || [],
    [timezones]
  );

  const settingsForm = useFormik({
    initialValues: {
      jira_link: settings?.jira_link || '',
      jira_token: settings?.jira_token || '',
      jira_time_zone: settings?.jira_time_zone || '',
      jira_email: settings?.jira_email || '',
      defect_removal_trend: settings?.defect_removal_trend || '10',
      jira_project_key: settings?.jira_project_key || '',
      jira_bug_status: settings?.jira_bug_status || '',
      jira_delivered_stories_statuses:
        settings?.jira_delivered_stories_statuses || '',
      jira_issue_status_open: settings?.jira_issue_status_open || '',
      jira_issue_status_close: settings?.jira_issue_status_close || '',
      jira_issue_status_reopen: settings?.jira_issue_status_reopen || '',
      jira_issue_priority_high: settings?.jira_issue_priority_high || '',
      jira_issue_priority_medium: settings?.jira_issue_priority_medium || '',
      jira_issue_priority_low: settings?.jira_issue_priority_low || '',
      jira_team_frontend: settings?.jira_team_frontend || '',
      jira_team_backend: settings?.jira_team_backend || '',
      jira_team_ios: settings?.jira_team_ios || '',
      jira_team_android: settings?.jira_team_android || ''
    },
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: settingsValidationSchema,
    onSubmit: async (settingsValues, { setFieldError }) => {
      mutate(
        {
          id: settings.id,
          ...settingsValues
        },
        {
          onSuccess: () => setJiraGroupsError(''),
          onError: (error) => {
            if (
              axios.isAxiosError(error) &&
              error?.response &&
              typeof error.response.data === 'object'
            ) {
              Object.entries<[string, [string]]>(error.response.data).forEach(
                ([key, [value]]) => {
                  if (key === 'jira_groups') {
                    setJiraGroupsError(value);
                  } else {
                    setFieldError(key, value);
                  }
                }
              );
            }
          }
        }
      );
    }
  });

  return isLoading ? (
    <Spinner />
  ) : (
    <form onSubmit={settingsForm.handleSubmit}>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_link}
          value={settingsForm.values.jira_link}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_link && settingsForm.errors.jira_link
              ? settingsForm.errors.jira_link
              : ''
          }
        />
        <Input
          {...settingsFields.jira_token}
          value={settingsForm.values.jira_token}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_token && settingsForm.errors.jira_token
              ? settingsForm.errors.jira_token
              : ''
          }
        />
        <Select
          {...settingsFields.jira_time_zone}
          value={{
            label: settingsForm.values.jira_time_zone,
            value: settingsForm.values.jira_time_zone
          }}
          options={timezonesOptions}
          onChange={(option) => {
            settingsForm.setFieldValue(
              settingsFields.jira_time_zone.name,
              option.value
            );
          }}
          onBlur={() =>
            settingsForm.setFieldTouched(settingsFields.jira_time_zone.name)
          }
          errorMsg={
            settingsForm.touched.jira_time_zone &&
            settingsForm.errors.jira_time_zone
              ? settingsForm.errors.jira_time_zone
              : ''
          }
          isDisabled={isLoadingTimezones}
        />
        <Input
          {...settingsFields.jira_email}
          value={settingsForm.values.jira_email}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_email && settingsForm.errors.jira_email
              ? settingsForm.errors.jira_email
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__item}>
        <Counter
          {...settingsFields.defect_removal_trend}
          value={settingsForm.values.defect_removal_trend}
          min={DEFECT_REMOVAL_TREND_MIN}
          onChange={(defectRemovalTrend: string) =>
            settingsForm.setFieldValue(
              settingsFields.defect_removal_trend.name,
              defectRemovalTrend
            )
          }
          onBlur={() =>
            settingsForm.setFieldTouched(
              settingsFields.defect_removal_trend.name
            )
          }
          errorMsg={
            settingsForm.touched.defect_removal_trend &&
            settingsForm.errors.defect_removal_trend
              ? settingsForm.errors.defect_removal_trend
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>Jira project Keys</div>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_project_key}
          value={settingsForm.values.jira_project_key}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_project_key &&
            settingsForm.errors.jira_project_key
              ? settingsForm.errors.jira_project_key
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>Jira bugs statuses</div>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_bug_status}
          value={settingsForm.values.jira_bug_status}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_bug_status &&
            settingsForm.errors.jira_bug_status
              ? settingsForm.errors.jira_bug_status
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>
        Jira delivered stories statuses
      </div>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_delivered_stories_statuses}
          value={settingsForm.values.jira_delivered_stories_statuses}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_delivered_stories_statuses &&
            settingsForm.errors.jira_delivered_stories_statuses
              ? settingsForm.errors.jira_delivered_stories_statuses
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>Jira issue statuses</div>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_issue_status_open}
          value={settingsForm.values.jira_issue_status_open}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_status_open &&
            settingsForm.errors.jira_issue_status_open
              ? settingsForm.errors.jira_issue_status_open
              : ''
          }
        />
        <Input
          {...settingsFields.jira_issue_status_close}
          value={settingsForm.values.jira_issue_status_close}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_status_close &&
            settingsForm.errors.jira_issue_status_close
              ? settingsForm.errors.jira_issue_status_close
              : ''
          }
        />
        <Input
          {...settingsFields.jira_issue_status_reopen}
          value={settingsForm.values.jira_issue_status_reopen}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_status_reopen &&
            settingsForm.errors.jira_issue_status_reopen
              ? settingsForm.errors.jira_issue_status_reopen
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>Jira issue priorities</div>
      <div className={styles.settings_form__line}>
        <Input
          {...settingsFields.jira_issue_priority_high}
          value={settingsForm.values.jira_issue_priority_high}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_priority_high &&
            settingsForm.errors.jira_issue_priority_high
              ? settingsForm.errors.jira_issue_priority_high
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__line}>
        <Input
          {...settingsFields.jira_issue_priority_medium}
          value={settingsForm.values.jira_issue_priority_medium}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_priority_medium &&
            settingsForm.errors.jira_issue_priority_medium
              ? settingsForm.errors.jira_issue_priority_medium
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__line}>
        <Input
          {...settingsFields.jira_issue_priority_low}
          value={settingsForm.values.jira_issue_priority_low}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_issue_priority_low &&
            settingsForm.errors.jira_issue_priority_low
              ? settingsForm.errors.jira_issue_priority_low
              : ''
          }
        />
      </div>
      <div className={styles.settings_form__title}>Jira teams</div>
      <div className={styles.settings_form__item}>
        <Input
          {...settingsFields.jira_team_frontend}
          value={settingsForm.values.jira_team_frontend}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_team_frontend &&
            settingsForm.errors.jira_team_frontend
              ? settingsForm.errors.jira_team_frontend
              : ''
          }
        />
        <Input
          {...settingsFields.jira_team_backend}
          value={settingsForm.values.jira_team_backend}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_team_backend &&
            settingsForm.errors.jira_team_backend
              ? settingsForm.errors.jira_team_backend
              : ''
          }
        />
        <Input
          {...settingsFields.jira_team_ios}
          value={settingsForm.values.jira_team_ios}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_team_ios &&
            settingsForm.errors.jira_team_ios
              ? settingsForm.errors.jira_team_ios
              : ''
          }
        />
        <Input
          {...settingsFields.jira_team_android}
          value={settingsForm.values.jira_team_android}
          onChange={settingsForm.handleChange}
          onBlur={settingsForm.handleBlur}
          errorMsg={
            settingsForm.touched.jira_team_android &&
            settingsForm.errors.jira_team_android
              ? settingsForm.errors.jira_team_android
              : ''
          }
        />
      </div>
      {jiraGroupsError && (
        <div
          style={{ color: THEME_COLOR.ERROR }}
          className={styles.settings_form__item__large}
        >
          {jiraGroupsError}
        </div>
      )}
      <Button
        type="submit"
        appearance="primary"
        isDisabled={!settingsForm.isValid || !settingsForm.dirty}
      >
        Save
      </Button>
    </form>
  );
}

export default SettingsForm;
