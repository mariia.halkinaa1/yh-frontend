import React, { useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { FieldArray, FormikProvider, getIn, useFormik } from 'formik';

import { Button, RowButtons } from '../../Buttons';
import Counter from '../../Counter';
import {
  Checkbox,
  DateTimePicker,
  Input,
  Select,
  Textarea
} from '../../Fields';
import Spinner from '../../Spinner';
import PATH from '../../../constants/routes';
import { ReportType } from '../../../constants/generateReport';
import { useCreateReport } from '../../../hooks';
import { sprintReportFields } from '../../../helpers/fields/qaReport';
import { sprintReportValidationSchema } from '../../../validation/qaReport';
import {
  getDateYearAgo,
  getDateYearAndDayAgo,
  getResolutionsByPlatforms,
  getTestrailData
} from '../helpers';
import {
  platformInitValues,
  testRailInitValues,
  SPRINT_MAX,
  SPRINT_MIN
} from '../constants';

import styles from './style.module.scss';

function SprintReportForm({
  projectKey,
  projectName,
  projectKeysOptions
}: {
  projectKey: string;
  projectName: string;
  projectKeysOptions: { value: string; label: string }[];
}) {
  const navigate = useNavigate();
  const { id: projectId } = useParams();
  const { isCreatingReport, createReport } = useCreateReport();

  const [minDate, setMinDate] = useState(getDateYearAndDayAgo(new Date()));

  const form = useFormik({
    initialValues: {
      sprint: '1',
      start: '',
      end: '',
      codefreeze: false,
      codefreezeDate: '',
      platforms: [platformInitValues],
      testRail: [testRailInitValues],
      problems: ''
    },
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: sprintReportValidationSchema,
    onSubmit: async ({
      sprint,
      start,
      end,
      codefreezeDate,
      platforms,
      testRail,
      problems
    }) => {
      const resolutionsByPlatforms = getResolutionsByPlatforms(platforms);
      const testRailData = getTestrailData(testRail);

      createReport(
        {
          report_type: ReportType.SPRINT_REPORT,
          project_key: projectKey,
          sprint_number: sprint,
          sprint_start_date: start,
          sprint_end_date: end,
          ...(codefreezeDate && {
            code_freeze_date: codefreezeDate
          }),
          resolutions_by_platforms: resolutionsByPlatforms,
          testrail_data: testRailData,
          problems
        },
        {
          onSuccess: ({ id }) =>
            navigate(
              `${PATH.PROJECT}/${projectId}${PATH.SPRINT_REPORT_PREVIEW}/${id}`,
              {
                state: {
                  projectName
                }
              }
            ),
        }
      );
    }
  });

  return isCreatingReport ? (
    <Spinner />
  ) : (
    <div className={styles.sprint_report}>
      <form className={styles.sprint_report__form} onSubmit={form.handleSubmit}>
        <div className={styles.sprint_report__form__item}>
          <Select
            {...sprintReportFields.projectKey}
            value={projectKeysOptions[0]}
            options={projectKeysOptions}
            isDisabled={true}
            isLoading={!projectName || !projectKey}
          />
          <Counter
            {...sprintReportFields.sprint}
            value={form.values.sprint}
            min={SPRINT_MIN}
            max={SPRINT_MAX}
            onChange={(sprint: string) =>
              form.setFieldValue(sprintReportFields.sprint.name, sprint)
            }
            onBlur={() => form.setFieldTouched(sprintReportFields.sprint.name)}
            errorMsg={
              form.touched.sprint && form.errors.sprint
                ? form.errors.sprint
                : ''
            }
          />
        </div>
        <div className={styles.sprint_report__form__item}>
          <DateTimePicker
            {...sprintReportFields.start}
            value={form.values.start}
            minDate={minDate}
            onChange={(date: string) => {
              form.setFieldValue(sprintReportFields.start.name, date);
            }}
            onBlur={() => form.setFieldTouched(sprintReportFields.start.name)}
            errorMsg={
              form.touched.start && form.errors.start ? form.errors.start : ''
            }
          />
          <DateTimePicker
            {...sprintReportFields.end}
            value={form.values.end}
            onChange={(date: string) => {
              form.setFieldValue(sprintReportFields.end.name, date);
              const dateYearAgo = getDateYearAgo(new Date(date));
              setMinDate(getDateYearAndDayAgo(new Date(date)));
              if (new Date(dateYearAgo) > new Date(form.values.start)) {
                form.setFieldValue(sprintReportFields.start.name, dateYearAgo);
              }
            }}
            onBlur={() => form.setFieldTouched(sprintReportFields.end.name)}
            errorMsg={
              form.touched.end && form.errors.end ? form.errors.end : ''
            }
          />
        </div>
        <div className={styles.sprint_report__form__item}>
          <Checkbox
            {...sprintReportFields.codefreeze}
            isChecked={form.values.codefreeze}
            onChange={form.handleChange}
            onBlur={form.handleBlur}
          />
          <DateTimePicker
            {...sprintReportFields.codefreezeDate}
            value={form.values.codefreezeDate}
            minDate={minDate}
            onChange={(date: string) => {
              form.setFieldValue(sprintReportFields.codefreezeDate.name, date);
            }}
            onBlur={() =>
              form.setFieldTouched(sprintReportFields.codefreezeDate.name)
            }
            errorMsg={
              form.touched.codefreezeDate && form.errors.codefreezeDate
                ? form.errors.codefreezeDate
                : ''
            }
            isDisabled={!form.values.codefreeze}
          />
        </div>
        <FormikProvider value={form}>
          <FieldArray
            name="platforms"
            render={({ name, push, remove }) => {
              return form.values.platforms.map(({ platform, build }, idx) => {
                const platformFieldName = `${name}.${idx}.platform`;
                const buildFieldName = `${name}.${idx}.build`;
                const platformFieldError = getIn(
                  form.errors,
                  platformFieldName
                );
                const platformFieldTouched = getIn(
                  form.touched,
                  platformFieldName
                );
                return (
                  <div
                    key={`${name}-${idx}`}
                    className={styles.sprint_report__form__item}
                  >
                    <Input
                      {...sprintReportFields.platform}
                      name={platformFieldName}
                      value={platform}
                      onChange={form.handleChange}
                      onBlur={form.handleBlur}
                      errorMsg={
                        platformFieldTouched && platformFieldError
                          ? platformFieldError
                          : ''
                      }
                    />
                    <Input
                      {...sprintReportFields.build}
                      name={buildFieldName}
                      value={build}
                      onChange={form.handleChange}
                      onBlur={form.handleBlur}
                    />
                    <div className={styles.sprint_report__form__row_buttons}>
                      <RowButtons
                        showRemoveButton={form.values.platforms.length > 1}
                        handleAddRow={() => push(platformInitValues)}
                        handleRemoveRow={() => remove(idx)}
                      />
                    </div>
                  </div>
                );
              });
            }}
          />
          <FieldArray
            name="testRail"
            render={({ name, push, remove }) => {
              return form.values.testRail.map(
                ({ testRun, testRailUrl }, idx) => {
                  const testRunFieldName = `${name}.${idx}.testRun`;
                  const testRailFieldName = `${name}.${idx}.testRailUrl`;
                  const testRailFieldError = getIn(
                    form.errors,
                    testRailFieldName
                  );
                  const testRailFieldTouched = getIn(
                    form.touched,
                    testRailFieldName
                  );
                  return (
                    <div
                      key={`${name}-${idx}`}
                      className={styles.sprint_report__form__item}
                    >
                      <Input
                        {...sprintReportFields.testRun}
                        name={testRunFieldName}
                        value={testRun}
                        onChange={form.handleChange}
                        onBlur={form.handleBlur}
                      />
                      <Input
                        {...sprintReportFields.testRailUrl}
                        name={testRailFieldName}
                        value={testRailUrl}
                        onChange={form.handleChange}
                        onBlur={form.handleBlur}
                        errorMsg={
                          testRailFieldTouched && testRailFieldError
                            ? testRailFieldError
                            : ''
                        }
                      />
                      <div className={styles.sprint_report__form__row_buttons}>
                        <RowButtons
                          showRemoveButton={form.values.testRail.length > 1}
                          handleAddRow={() => push(testRailInitValues)}
                          handleRemoveRow={() => remove(idx)}
                        />
                      </div>
                    </div>
                  );
                }
              );
            }}
          />
        </FormikProvider>
        <div className={styles.sprint_report__form__item}>
          <div className={styles.sprint_report__form__item__large}>
            <Textarea
              {...sprintReportFields.problems}
              value={form.values.problems}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              errorMsg={
                form.touched.problems && form.errors.problems
                  ? form.errors.problems
                  : ''
              }
            />
          </div>
        </div>
        <Button
          type="submit"
          appearance="primary"
          isDisabled={!form.isValid || !form.dirty}
        >
          Generate Report
        </Button>
      </form>
    </div>
  );
}

export default SprintReportForm;
