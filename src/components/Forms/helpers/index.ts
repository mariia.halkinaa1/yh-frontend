import { StylesConfig } from '@atlaskit/select';

import getResolutionColor from '../../../utils/getResolutionColor';
import { Resolutions } from '../../../constants/generateReport';

export const customSelectStyles: StylesConfig = {
  option: (provided, { data: { value } }) => {
    return {
      ...provided,
      color: getResolutionColor(value as Resolutions)
    };
  }
};

export const getResolutionsByPlatforms = (
  platforms: { platform: string; build?: string }[]
) => {
  return platforms.map(({ platform, build }) => {
    return {
      platform,
      ...(build && {
        build_number: build
      })
    };
  });
};

export const getTestrailData = (
  testRailData: { testRun?: string; testRailUrl: string }[]
) => {
  return testRailData.map(({ testRun, testRailUrl }) => {
    return {
      testrail_url: testRailUrl,
      ...(testRun && {
        test_run: testRun
      })
    };
  });
};

export const getDateYearAgo = (day: Date) => {
  const oneYearFromNow = day;
  oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() - 1);
  return oneYearFromNow.toISOString();
};

export const getDateYearAndDayAgo = (day: Date) => {
  const oneYearFromNow = day;
  oneYearFromNow.setFullYear(oneYearFromNow.getFullYear() - 1);
  oneYearFromNow.setDate(oneYearFromNow.getDate() - 1);
  return oneYearFromNow.toISOString();
};
