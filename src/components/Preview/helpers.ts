import { BugStatus, IDefectRemovalTrend } from '../../api/projects/types';
import { TableType, headers, drtDatasets } from './constants';

export const getRows = (
  items: { [key: string]: string }[],
  type: TableType
) => {
  const head = headers[type];
  const rows = items.map((item) => head.map((h) => item[h] ?? '-'));

  return rows.length ? [head, ...rows] : [];
};

interface IDRTValues {
  [key: string]: number[];
}

export const getDrtData = (drt?: IDefectRemovalTrend) => {
  const labels: string[] = [];
  const values: IDRTValues = {
    [BugStatus.Backlog]: [],
    [BugStatus.Closed]: [],
    [BugStatus.Opened]: []
  };

  drt?.forEach((drtItem) => {
    drtItem.forEach((item) => {
      if (item?.sprint_number) {
        labels.push(item.sprint_number);
        return;
      }

      if (item) {
        values[item.status].push(item.total);
      } else {
        values[BugStatus.Backlog].push(0);
        values[BugStatus.Closed].push(0);
        values[BugStatus.Opened].push(0);
      }
    });
  });

  return { labels, values };
};

export const getDrtDatasets = (values: IDRTValues) => [
  {
    ...drtDatasets.backlog,
    data: values[BugStatus.Backlog]
  },
  {
    ...drtDatasets.opened,
    data: values[BugStatus.Opened]
  },
  {
    ...drtDatasets.closed,
    data: values[BugStatus.Closed]
  }
];

export const isZeroDrtValues = (values: IDRTValues) =>
  Object.entries(values).every(([, value]) => value.every((v) => !v));
