import React, { useCallback, useContext, useMemo, useRef } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormik, FormikProvider, FieldArray, getIn } from 'formik';
import { ButtonGroup } from '@atlaskit/button';
import { toBlob } from 'html-to-image';

import { Button } from '../Buttons';
import { DefectRemovalTrend } from '../Charts';
import { Select } from '../Fields';
import Spinner from '../Spinner';
import {
  TBugDynamics,
  TBugStatistic,
  TReportDataResponse
} from '../../api/report/types';
import PATH from '../../constants/routes';
import { customSelectStyles } from '../Forms/helpers';
import { resolutions } from '../../constants/generateReport';
import NotificationContext from '../../context/notificationContext';
import resolutionFields from '../../helpers/fields/resolutionFields';
import errorMsg from '../../helpers/messages/errorMsg';
import { useDRT, useGenerateReport, useRemoveReport } from '../../hooks';
import resolutionsValidationSchema from '../../validation/resolutions';
import handleDownloadPdf from '../../utils/handleDownloadPdf';
import {
  BugDynamics,
  BugStatistic,
  DeliveredStories,
  ListOfBugs
} from './components';
import { getDrtData, getDrtDatasets, isZeroDrtValues } from './helpers';

import styles from './styles.module.scss';

interface IPreview {
  data: {
    jiraProjectKey: string;
    sprintNumber: string;
    sprintStartDate: string;
    sprintEndDate: string;
    deliveryDate?: string;
    codefreezeDate?: string;
    problems?: string;
    releaseNotes?: string;
    listOfBugs: TReportDataResponse;
    deliveredStories: TReportDataResponse;
    bugDynamics: TBugDynamics;
    bugStatistic: TBugStatistic;
    resolutionsByPlatforms: {
      platform: string;
      build_number?: string;
    }[];
    testRailData?: {
      test_run?: string;
      testrail_url: string;
    }[];
  };
  isDeliveryReport: boolean;
}

const Preview = ({
  data: {
    jiraProjectKey,
    sprintNumber,
    sprintStartDate,
    sprintEndDate,
    deliveryDate,
    codefreezeDate,
    resolutionsByPlatforms,
    testRailData,
    problems,
    releaseNotes,
    listOfBugs,
    deliveredStories,
    bugDynamics,
    bugStatistic
  },
  isDeliveryReport
}: IPreview) => {
  const chartRef = useRef<HTMLDivElement>(null);
  const navigate = useNavigate();
  const { id: projectId, reportId } = useParams();
  const { onError } = useContext(NotificationContext);
  const { isGeneratingReport, generateReport } = useGenerateReport();
  const { removeReport } = useRemoveReport(reportId as string);
  const { isLoadingDRT, drt } = useDRT(projectId as string);

  const drtChartData = useMemo(() => {
    if (!drt || drt.length === 0) return;

    const { values, labels } = getDrtData(drt);

    if (isZeroDrtValues(values)) return;

    return { labels, datasets: getDrtDatasets(values) };
  }, [drt]);

  const initialResolutions = useMemo(
    () => resolutionsByPlatforms.map(() => ''),
    [resolutionsByPlatforms]
  );

  const handleTakeScreenshot = useCallback(async () => {
    if (chartRef.current === null || isLoadingDRT) {
      return;
    }
    const blob = await toBlob(chartRef.current as HTMLDivElement);
    const file = new File([blob as Blob], 'file.png');
    return file;
  }, [chartRef, isLoadingDRT]);

  const onSuccessGenerateReport = useCallback(
    async (pdfUrl: string, isToDownloadPdf: boolean) => {
      if (isToDownloadPdf) {
        await handleDownloadPdf(pdfUrl);
      }
      navigate(`${PATH.PROJECT}/${projectId}`);
    },
    [projectId, navigate]
  );

  const onCancel = useCallback(async () => {
    await removeReport();
    navigate(`${PATH.PROJECT}/${projectId}`);
  }, [removeReport, navigate, projectId]);

  const onSubmit = useCallback(
    async (
      { resolutions: values }: { resolutions: string[] },
      isToDownloadPdf = true
    ) => {
      const id = reportId as string;

      const resolutionData = JSON.stringify(
        resolutionsByPlatforms.map((data, idx) => ({
          ...data,
          resolution: values[idx]
        }))
      );

      if (isDeliveryReport || !drtChartData) {
        return generateReport(
          {
            id,
            resolutions_by_platforms: resolutionData
          },
          {
            onSuccess: ({ pdf_url: pdfUrl }) =>
              onSuccessGenerateReport(pdfUrl, isToDownloadPdf)
          }
        );
      }
      const file = await handleTakeScreenshot();
      if (!file) {
        onError(errorMsg.DRTChartScreenshot);
        return;
      }
      return generateReport(
        {
          id,
          resolutions_by_platforms: resolutionData,
          file: file as File
        },
        {
          onSuccess: ({ pdf_url: pdfUrl }) =>
            onSuccessGenerateReport(pdfUrl, isToDownloadPdf)
        }
      );
    },
    [
      isDeliveryReport,
      reportId,
      resolutionsByPlatforms,
      drtChartData,
      generateReport,
      handleTakeScreenshot,
      onError,
      onSuccessGenerateReport
    ]
  );

  const form = useFormik({
    initialValues: {
      resolutions: initialResolutions
    },
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: resolutionsValidationSchema,
    onSubmit: (values) => onSubmit(values)
  });

  return isGeneratingReport ? (
    <Spinner />
  ) : (
    <form onSubmit={form.handleSubmit}>
      <div className={styles.preview_container}>
        <div className={styles.preview_item__row}>
          <div className={styles.preview_item__xsmall}>
            <span className={styles.preview_item__title}>Project key:</span>
            {jiraProjectKey}
          </div>
        </div>
        <div className={styles.preview_item__row}>
          <div className={styles.preview_item__xsmall}>
            <span className={styles.preview_item__title}>Sprint:</span>
            {sprintNumber}
          </div>
        </div>
        <div className={styles.preview_item__row}>
          <div className={styles.preview_item__xsmall}>
            <div className={styles.preview_item__title}>
              Start date and time:
            </div>
            {sprintStartDate}
          </div>
          <div className={styles.preview_item__xsmall}>
            <div className={styles.preview_item__title}>End date and time:</div>
            {sprintEndDate}
          </div>
          {isDeliveryReport && !!deliveryDate && (
            <div className={styles.preview_item__small}>
              <div className={styles.preview_item__title}>
                Delivery date and time:
              </div>
              {deliveryDate}
            </div>
          )}
          {!isDeliveryReport && !!codefreezeDate && (
            <>
              <div className={styles.preview_item__xsmall}>
                <input type="checkbox" checked={true} readOnly />
                <label htmlFor="">Codefreeze was</label>
              </div>
              <div className={styles.preview_item__xsmall}>
                <div className={styles.preview_item__title}>
                  Codefreeze date and time:
                </div>
                {codefreezeDate}
              </div>
            </>
          )}
        </div>
        <FormikProvider value={form}>
          <FieldArray
            name="resolutions"
            render={({ name }) => {
              return resolutionsByPlatforms?.map(
                ({ platform, build_number }, idx) => {
                  const resolutionFieldName = `${name}.${idx}`;
                  const resolutionFieldError = getIn(
                    form.errors,
                    resolutionFieldName
                  );
                  const resolutionFieldTouched = getIn(
                    form.touched,
                    resolutionFieldName
                  );

                  return (
                    <div
                      key={`${platform}-${idx}`}
                      className={styles.preview_item__row}
                    >
                      <div
                        className={`${styles.preview_item__xsmall} ${styles.preview_item__topmargin}`}
                      >
                        <div className={styles.preview_item__title}>
                          Platform:
                        </div>
                        {platform}
                      </div>
                      <Select
                        {...resolutionFields.resolution}
                        value={{
                          label:
                            resolutions.find(
                              (r) => r.value === form.values.resolutions[idx]
                            )?.label || '',
                          value: form.values.resolutions[idx]
                        }}
                        styles={customSelectStyles}
                        options={resolutions}
                        onChange={(option) => {
                          form.setFieldValue(resolutionFieldName, option.value);
                        }}
                        onBlur={() => form.setFieldTouched(resolutionFieldName)}
                        errorMsg={
                          resolutionFieldTouched && resolutionFieldError
                            ? resolutionFieldError
                            : ''
                        }
                      />
                      {!!build_number && (
                        <div
                          className={`${styles.preview_item__xsmall} ${styles.preview_item__topmargin}`}
                        >
                          <div className={styles.preview_item__title}>
                            Build number:
                          </div>
                          {build_number}
                        </div>
                      )}
                    </div>
                  );
                }
              );
            }}
          />
        </FormikProvider>
        <BugStatistic
          bugStatistic={bugStatistic}
          isDeliveryReport={isDeliveryReport}
        />

        <div className={styles.preview_item__row}>
          <ListOfBugs listOfBugs={listOfBugs} />
        </div>

        {!isDeliveryReport && (
          <div className={styles.preview_item__row}>
            <BugDynamics bugDynamics={bugDynamics} />
          </div>
        )}

        {!isDeliveryReport &&
          testRailData?.map(({ test_run, testrail_url }, idx) => (
            <div
              key={`${testrail_url}-${idx}`}
              className={styles.preview_item__row}
            >
              {!!test_run && (
                <div className={styles.preview_item__small}>
                  <div className={styles.preview_item__title}>
                    Milestone/TestRun:
                  </div>
                  {test_run}
                </div>
              )}
              <div className={styles.preview_item__small}>
                <div className={styles.preview_item__title}>
                  Test Rail Link:
                </div>
                {testrail_url}
              </div>
            </div>
          ))}

        {!isDeliveryReport && !!problems && (
          <div className={styles.preview_item__large}>
            <div className={styles.preview_item__title}>Summary:</div>
            <div className={styles.report_notes}>{problems}</div>
          </div>
        )}

        <div className={styles.preview_item__row}>
          <DeliveredStories deliveredStories={deliveredStories} />
        </div>

        {isDeliveryReport && !!releaseNotes && (
          <div className={styles.preview_item__large}>
            <div className={styles.preview_item__title}>Release notes:</div>
            <div className={styles.report_notes}> {releaseNotes}</div>
          </div>
        )}

        {!isDeliveryReport && (
          <div className={styles.preview_item__row}>
            <div className={styles.preview_item__medium} ref={chartRef}>
              <div className={styles.preview_item__title}>
                Defect removal trend:
              </div>
              {isLoadingDRT ? (
                <Spinner />
              ) : (
                <DefectRemovalTrend data={drtChartData} />
              )}
            </div>
          </div>
        )}
      </div>
      <ButtonGroup>
        <Button
          type="submit"
          appearance="primary"
          isDisabled={!form.isValid || !form.dirty}
        >
          Save as pdf file
        </Button>
        <Button appearance="default" type="button" onClick={onCancel}>
          Cancel
        </Button>
        {!isDeliveryReport && (
          <Button
            appearance="default"
            type="button"
            isDisabled={!form.isValid || !form.dirty}
            onClick={() => onSubmit(form.values, false)}
          >
            OK
          </Button>
        )}
      </ButtonGroup>
    </form>
  );
};

export default Preview;
