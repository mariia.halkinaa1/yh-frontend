import { BugStatus } from '../../api/projects/types';

export enum TableType {
  BugStatisticPriority = 'BugStatisticPriority',
  BugStatisticSeverity = 'BugStatisticSeverity',
  ListOfBugs = 'ListOfBugs',
  BugsDynamics = 'BugsDynamics',
  DeliveredStories = 'DeliveredStories'
}

export const headers = {
  [TableType.BugStatisticPriority]: [
    'priority',
    'android',
    'ios',
    'frontend',
    'backend',
    'other',
    'total'
  ],
  [TableType.BugStatisticSeverity]: [
    'severity',
    'android',
    'ios',
    'frontend',
    'backend',
    'other',
    'total'
  ],
  [TableType.ListOfBugs]: ['key', 'priority', 'severity', 'summary'],
  [TableType.BugsDynamics]: [
    'status',
    'Before code freeze',
    'After code freeze',
    'total'
  ],
  [TableType.DeliveredStories]: ['key', 'summary']
};

export const DRT_COLORS = {
  BACKLOG: 'rgb(242, 192, 68)',
  OPENED: 'rgb(96, 141, 238)',
  CLOSED: 'rgb(139, 178, 113)'
};

export const drtDatasets = {
  backlog: {
    type: 'line' as const,
    label: BugStatus.Backlog,
    borderWidth: 4,
    pointRadius: 0,
    borderColor: DRT_COLORS.BACKLOG,
    backgroundColor: DRT_COLORS.BACKLOG
  },
  opened: {
    type: 'bar' as const,
    label: BugStatus.Opened,
    backgroundColor: DRT_COLORS.OPENED
  },
  closed: {
    type: 'bar' as const,
    label: BugStatus.Closed,
    backgroundColor: DRT_COLORS.CLOSED
  }
};
