import React from 'react';

import THEME_COLOR from '../../../../constants/themeColor';

import styles from '../../styles.module.scss';

const TableError = ({
  title,
  tableName,
  tableError
}: {
  title: JSX.Element;
  tableName: string;
  tableError?: string;
}) => {
  return (
    <div className={styles.preview_item__large}>
      {title}
      <div style={{ color: THEME_COLOR.ERROR }}>
        {tableError
          ? `${tableName} table has not been created because - ${tableError}`
          : `${tableName} table has not been created.`}
      </div>
    </div>
  );
};

export default TableError;
