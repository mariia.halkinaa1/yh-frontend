import React from 'react';

import styles from './styles.module.scss';

const CustomTable = ({ rows }: { rows: (string | null)[][] }) => {
  const [head, ...row] = rows;

  const headRow = head.map((th) => {
    return th === 'ios' ? (
      <th style={{ textTransform: 'none' }} key={th}>
        iOS
      </th>
    ) : (
      <th key={th}>{th}</th>
    );
  });

  const bodyRow = row.map((tr, index) => {
    return (
      <tr key={`${head[index]}-${index}`}>
        {tr.map((td, idx) => {
          return <td key={`${tr[0]}-${idx}`}>{td ?? '-'}</td>;
        })}
      </tr>
    );
  });

  return (
    <table className={styles.custom_table}>
      <thead>
        <tr>{headRow}</tr>
      </thead>
      <tbody>{bodyRow}</tbody>
    </table>
  );
};

export default CustomTable;
