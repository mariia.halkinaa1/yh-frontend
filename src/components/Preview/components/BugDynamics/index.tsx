import React from 'react';

import { TBugDynamics } from '../../../../api/report/types';
import { getRows } from '../../helpers';
import { TableType } from '../../constants';
import CustomTable from '../CustomTable';
import NoData from '../NoData';
import TableError from '../TableError';

import styles from '../../styles.module.scss';

const BugDynamics = ({ bugDynamics }: { bugDynamics: TBugDynamics }) => {
  const tableName = 'Bugs dynamics during the sprint';
  const title = <div className={styles.preview_item__title}>{tableName}:</div>;

  if (bugDynamics.textError) {
    return (
      <TableError
        title={title}
        tableName={tableName}
        tableError={bugDynamics.textError}
      />
    );
  }

  const values = Object.entries(bugDynamics.data).map(([, value]) => value);
  const bugDynamicsRows = getRows(values, TableType.BugsDynamics);

  if (bugDynamicsRows.length === 0) {
    return <NoData title={title} />;
  }

  return (
    <div className={styles.preview_item__small}>
      {title}
      <CustomTable rows={bugDynamicsRows} />
    </div>
  );
};

export default BugDynamics;
