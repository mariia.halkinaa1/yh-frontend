import React from 'react';

import { TBugStatistic } from '../../../../api/report/types';
import { TableType } from '../../constants';
import { getRows } from '../../helpers';
import CustomTable from '../CustomTable';
import NoData from '../NoData';
import TableError from '../TableError';

import styles from '../../styles.module.scss';

const BugStatistic = ({
  bugStatistic,
  isDeliveryReport
}: {
  bugStatistic: TBugStatistic;
  isDeliveryReport: boolean;
}) => {
  const tableName = 'Bug statistic at the end of the sprint';
  const title = <div className={styles.preview_item__title}>{tableName}:</div>;

  if (bugStatistic.textError) {
    return (
      <TableError
        title={title}
        tableName={tableName}
        tableError={bugStatistic.textError}
      />
    );
  }

  const dataPriorityRows = getRows(
    bugStatistic.render_data_priority,
    TableType.BugStatisticPriority
  );
  const dataSeverityRows = getRows(
    bugStatistic.render_data_severity,
    TableType.BugStatisticSeverity
  );

  if (dataPriorityRows.length === 0 || dataSeverityRows.length === 0) {
    return <NoData title={title} />;
  }

  return (
    <div className={styles.preview_item__large}>
      {title}
      <div className={styles.preview_item__row}>
        <div className={styles.preview_item__small}>
          <CustomTable rows={dataPriorityRows} />
        </div>
        {!isDeliveryReport && (
          <div className={styles.preview_item__small}>
            <CustomTable rows={dataSeverityRows} />
          </div>
        )}
      </div>
    </div>
  );
};

export default BugStatistic;
