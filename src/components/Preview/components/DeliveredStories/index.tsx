import React from 'react';

import { TReportDataResponse } from '../../../../api/report/types';
import { TableType } from '../../constants';
import { getRows } from '../../helpers';
import CustomTable from '../CustomTable';
import NoData from '../NoData';
import TableError from '../TableError';

import styles from '../../styles.module.scss';

const DeliveredStories = ({
  deliveredStories
}: {
  deliveredStories: TReportDataResponse;
}) => {
  const tableName = 'Delivered stories';
  const title = <div className={styles.preview_item__title}>{tableName}:</div>;

  if (deliveredStories.textError) {
    return (
      <TableError
        title={title}
        tableName={tableName}
        tableError={deliveredStories.textError}
      />
    );
  }

  const deliveredStoriesRows = getRows(
    deliveredStories.data,
    TableType.DeliveredStories
  );

  if (deliveredStoriesRows.length === 0) {
    return <NoData title={title} />;
  }

  return (
    <div className={styles.preview_item__medium}>
      {title}
      <CustomTable rows={deliveredStoriesRows} />
    </div>
  );
};

export default DeliveredStories;
