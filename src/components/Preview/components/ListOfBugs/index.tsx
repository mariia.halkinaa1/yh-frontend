import React from 'react';

import { TReportDataResponse } from '../../../../api/report/types';
import { TableType } from '../../constants';
import { getRows } from '../../helpers';
import CustomTable from '../CustomTable';
import NoData from '../NoData';
import TableError from '../TableError';

import styles from '../../styles.module.scss';

const ListOfBugs = ({ listOfBugs }: { listOfBugs: TReportDataResponse }) => {
  const tableName = 'List of bugs';
  const title = <div className={styles.preview_item__title}>{tableName}:</div>;

  if (listOfBugs.textError) {
    return (
      <TableError
        title={title}
        tableName={tableName}
        tableError={listOfBugs.textError}
      />
    );
  }

  const listOfBugsRows = getRows(listOfBugs.data, TableType.ListOfBugs);

  if (listOfBugsRows.length === 0) {
    return <NoData title={title} />;
  }

  return (
    <div className={styles.preview_item__medium}>
      {title}
      <CustomTable rows={listOfBugsRows} />
    </div>
  );
};

export default ListOfBugs;
