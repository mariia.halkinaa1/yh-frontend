import React from 'react';

import styles from '../../styles.module.scss';

const NoData = ({ title }: { title: JSX.Element }) => {
  return (
    <div className={styles.preview_item__large}>
      {title}
      <div className={styles.preview_item__large}>
        There is no data to display.
      </div>
    </div>
  );
};

export default NoData;
