import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';

import PATH from '../../constants/routes';
import Dashboard from '../../pages/Dashboard';
import Projects from '../../pages/Projects';
import Report from '../../pages/Report';
import ReportPreview from '../../pages/ReportPreview';
import Project from '../../pages/Project';
import Settings from '../../pages/Settings';

import styles from './style.module.scss';

const MainContent = () => {
  return (
    <div className={styles.content_container}>
      <Routes>
        <Route index element={<Navigate to={PATH.PROJECTS} />} />
        <Route path={PATH.DASHBOARD} element={<Dashboard />}>
          <Route path={PATH.PROJECTS} element={<Projects />} />
          <Route path={PATH.SETTINGS} element={<Settings />} />
        </Route>
        <Route path={`${PATH.PROJECT}/:id`}>
          <Route index element={<Project />} />
          <Route
            path={`${PATH.PROJECT}/:id${PATH.SPRINT_REPORT}`}
            element={<Report />}
          />
          <Route
            path={`${PATH.PROJECT}/:id${PATH.DELIVERY_REPORT}`}
            element={<Report />}
          />
          <Route
            path={`${PATH.PROJECT}/:id${PATH.SPRINT_REPORT_PREVIEW}/:reportId`}
            element={<ReportPreview />}
          />
          <Route
            path={`${PATH.PROJECT}/:id${PATH.DELIVERY_REPORT_PREVIEW}/:reportId`}
            element={<ReportPreview />}
          />
        </Route>
        <Route path="*" element={<Navigate to={PATH.PROJECTS} replace />} />
      </Routes>
    </div>
  );
};

export default MainContent;
