import { SortOrder } from '../constants';

export const REPORTS_PER_PAGE = 10;

export const INITIAL_PAGE = 1;

export const INITIAL_SORT_KEY = 'publish_date';

export const INITIAL_SORT_ORDER = SortOrder.DESC;

export const TableHeaderData = {
  cells: [
    { content: 'Publish date', key: 'publish_date', isSortable: true },
    { content: 'Project key', key: 'project_key', isSortable: true },
    { content: 'Sprint number', key: 'sprint_number', isSortable: true },
    {
      content: 'Start date of the sprint',
      key: 'sprint_start_date',
      isSortable: true
    },
    {
      content: 'End date of the sprint',
      key: 'sprint_end_date',
      isSortable: true
    },
    { content: 'Reporter', key: 'reporter_name', isSortable: true },
    { content: '', isSortable: false }
  ]
};

export const EMPTY_VIEW = 'QA reports have not been published yet';
