import React, { useMemo, useState } from 'react';
import { DynamicTableStateless } from '@atlaskit/dynamic-table';
import Pagination from '@atlaskit/pagination';

import { SavePdfButton } from '../../Buttons';
import { useReports } from '../../../hooks';
import { formatDate } from '../../../utils/formatDate';
import handleDownloadPdf from '../../../utils/handleDownloadPdf';
import { SortOrderType } from '../types';
import {
  TableHeaderData,
  REPORTS_PER_PAGE,
  INITIAL_PAGE,
  INITIAL_SORT_KEY,
  INITIAL_SORT_ORDER,
  EMPTY_VIEW
} from './constants';

import styles from '../style.module.scss';

const ReportsTable = ({
  projectKey,
  projectJiraLink
}: {
  projectKey: string;
  projectJiraLink: string;
}) => {
  const [page, setPage] = useState<number>(INITIAL_PAGE);
  const [sortKey, setSortKey] = useState<string>(INITIAL_SORT_KEY);
  const [sortOrder, setSortOrder] = useState<SortOrderType>(INITIAL_SORT_ORDER);
  const {
    isLoading,
    results: reports,
    count = 0
  } = useReports(!!projectKey && !!projectJiraLink, {
    page,
    sortKey,
    sortOrder,
    filter: {
      projectKey,
      projectJiraLink
    }
  });

  const countPages = useMemo(
    () => Math.ceil(count / REPORTS_PER_PAGE),
    [count]
  );

  const pages = useMemo(
    () => Array.from({ length: countPages }, (v, i) => i + 1),
    [countPages]
  );

  const onSetPage = (pageNumber: number) => {
    setPage(pageNumber);
  };

  const onSort = ({
    key,
    sortOrder: order
  }: {
    key: string;
    sortOrder: SortOrderType;
  }) => {
    setSortKey(key);
    setSortOrder(order);
  };

  const rows = useMemo(
    () =>
      reports.map(
        (
          {
            id: reportId,
            publish_date: publishDate,
            project_key: jiraProjectKey,
            sprint_number: sprintNumber,
            sprint_start_date: startDateFrame,
            sprint_end_date: endDateFrame,
            reporter_name: reporterName,
            pdf_url: pdfUrl
          },
          index
        ) => {
          return {
            key: reportId,
            cells: [
              { content: formatDate(publishDate), key: publishDate + index },
              { content: jiraProjectKey, key: jiraProjectKey + index },
              {
                content: sprintNumber,
                key: sprintNumber + index
              },
              {
                content: formatDate(startDateFrame),
                key: startDateFrame + index
              },
              { content: formatDate(endDateFrame), key: endDateFrame + index },
              {
                content: reporterName || 'user deleted',
                key: reporterName + index
              },
              {
                content: (
                  <SavePdfButton
                    onSaveReport={() => handleDownloadPdf(pdfUrl)}
                  />
                )
              }
            ]
          };
        }
      ),
    [reports]
  );

  return (
    <div className={styles.table_container}>
      <DynamicTableStateless
        head={TableHeaderData}
        rows={rows}
        rowsPerPage={undefined}
        sortKey={sortKey}
        sortOrder={sortOrder}
        isLoading={isLoading}
        isFixedSize
        emptyView={<>{EMPTY_VIEW}</>}
        loadingSpinnerSize="large"
        onSort={onSort}
      />
      {countPages > 1 && (
        <div className={styles.pagination_container}>
          <Pagination
            pages={pages}
            selectedIndex={page && page - 1}
            onChange={(e, p) => onSetPage(p)}
          />
        </div>
      )}
    </div>
  );
};

export default ReportsTable;
