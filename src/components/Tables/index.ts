import ReportsTable from './ReportsTable';
import ProjectsTable from './ProjectsTable';

export { ReportsTable, ProjectsTable };
