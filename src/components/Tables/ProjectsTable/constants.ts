import { SortOrder } from '../constants';

export const PROJECTS_PER_PAGE = 10;

export const INITIAL_PAGE = 1;

export const INITIAL_SORT_KEY = 'updated_at';

export const INITIAL_SORT_ORDER = SortOrder.DESC;

export const TableHeaderData = {
  cells: [
    { content: 'Updated', key: 'updated_at', width: 15, isSortable: true },
    { content: 'Project key', key: 'project_key', isSortable: true },
    {
      content: 'Quality baseline',
      key: 'open_bugs.Quality',
      isSortable: true
    },
    {
      content: 'High priority issues',
      key: 'open_bugs.High',
      isSortable: true
    },
    {
      content: 'Medium priority issues',
      key: 'open_bugs.Medium',
      isSortable: true
    },
    {
      content: 'Low priority issues',
      key: 'open_bugs.Low',
      isSortable: true
    }
  ]
};

export const EMPTY_VIEW = 'No projects yet';
