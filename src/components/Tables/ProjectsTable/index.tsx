import React, { useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { DynamicTableStateless } from '@atlaskit/dynamic-table';
import Pagination from '@atlaskit/pagination';

import PATH from '../../../constants/routes';
import { useProjects } from '../../../hooks';
import { formatDate } from '../../../utils/formatDate';
import { SortOrderType } from '../types';
import {
  TableHeaderData,
  PROJECTS_PER_PAGE,
  INITIAL_PAGE,
  INITIAL_SORT_KEY,
  INITIAL_SORT_ORDER,
  EMPTY_VIEW
} from './constants';

import styles from '../style.module.scss';

const ProjectsTable = () => {
  const navigate = useNavigate();

  const [page, setPage] = useState<number>(INITIAL_PAGE);
  const [sortKey, setSortKey] = useState<string>(INITIAL_SORT_KEY);
  const [sortOrder, setSortOrder] = useState<SortOrderType>(INITIAL_SORT_ORDER);
  const {
    isLoading,
    results: projects,
    count = 0
  } = useProjects({
    page,
    sortKey,
    sortOrder
  });

  const countPages = useMemo(
    () => Math.ceil(count / PROJECTS_PER_PAGE),
    [count]
  );

  const pages = useMemo(
    () => Array.from({ length: countPages }, (v, i) => i + 1),
    [countPages]
  );

  const onSetPage = (pageNumber: number) => {
    setPage(pageNumber);
  };

  const onSort = ({
    key,
    sortOrder: order
  }: {
    key: string;
    sortOrder: SortOrderType;
  }) => {
    setSortKey(key);
    setSortOrder(order);
  };

  const rows = useMemo(
    () =>
      projects.map(
        (
          {
            id: projectId,
            project_key: projectKey,
            updated_at: updatedAt,
            open_bugs: {
              Quality: qualityBaseline,
              High: jiraIssuePriorityHigh,
              Medium: jiraIssuePriorityMedium,
              Low: jiraIssuePriorityLow
            }
          },
          index
        ) => {
          return {
            key: projectId,
            onClick: () => {
              navigate(`${PATH.PROJECT}/${projectId}`);
            },
            cells: [
              {
                content: formatDate(updatedAt),
                key: updatedAt + index
              },
              { content: projectKey, key: projectKey + index },
              {
                content: qualityBaseline,
                key: `qualityBaseline${index}`
              },
              {
                content: jiraIssuePriorityHigh,
                key: `jiraIssuePriorityHigh${index}`
              },
              {
                content: jiraIssuePriorityMedium,
                key: `jiraIssuePriorityMedium${index}`
              },
              {
                content: jiraIssuePriorityLow,
                key: `jiraIssuePriorityLow${index}`
              }
            ]
          };
        }
      ),
    [projects, navigate]
  );

  return (
    <div
      className={`${styles.table_container} ${styles.projects_table_container}`}
    >
      <DynamicTableStateless
        head={TableHeaderData}
        rows={rows}
        rowsPerPage={undefined}
        sortKey={sortKey}
        sortOrder={sortOrder}
        isLoading={isLoading}
        isFixedSize
        emptyView={<>{EMPTY_VIEW}</>}
        loadingSpinnerSize="large"
        onSort={onSort}
      />
      {countPages > 1 && (
        <div className={styles.pagination_container}>
          <Pagination
            pages={pages}
            selectedIndex={page && page - 1}
            onChange={(e, p) => onSetPage(p)}
          />
        </div>
      )}
    </div>
  );
};

export default ProjectsTable;
