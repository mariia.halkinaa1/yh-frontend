import React, { useContext } from 'react';

import ErrorIcon from '@atlaskit/icon/glyph/error';
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import FlagAK from '@atlaskit/flag';

import NotificationContext, { TYPE } from '../../context/notificationContext';
import THEME_COLOR from '../../constants/themeColor';
import styles from './style.module.scss';

const Notification = () => {
  const {
    type,
    notification: { title, description }
  } = useContext(NotificationContext);
  if (!type) {
    return null;
  }
  return (
    <div className={styles.flag_container}>
      <FlagAK
        id={title}
        title={title}
        description={description}
        appearance="normal"
        icon={
          type === TYPE.ERROR ? (
            <ErrorIcon label="Error" primaryColor={THEME_COLOR.ERROR} />
          ) : (
            <SuccessIcon label="Success" primaryColor={THEME_COLOR.SUCCESS} />
          )
        }
      />
    </div>
  );
};

export default Notification;
