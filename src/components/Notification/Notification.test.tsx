import React, { useContext } from 'react';
import { render, screen } from '@testing-library/react';

import NotificationContext, {
  NotificationProvider,
  INotificationData
} from '../../context/notificationContext';

import Notification from './';

const Button = ({
  notification,
  isSuccess = true
}: {
  notification: INotificationData;
  isSuccess?: boolean;
}) => {
  const { onSuccess, onError } = useContext(NotificationContext);

  return (
    <button
      onClick={() =>
        isSuccess ? onSuccess(notification) : onError(notification)
      }
    >
      Button
    </button>
  );
};

it('renders <Notification />', () => {
  const notification = {
    title: 'Notification Title',
    description: 'Notification Description'
  };

  render(
    <NotificationProvider>
      <>
        <Notification />
        <Button notification={notification} />
      </>
    </NotificationProvider>
  );

  screen.getByRole('button', { name: /Button/i }).click();

  expect(screen.getByRole('alert')).toBeInTheDocument();
});

it('renders title and description in the <Notification />', () => {
  const notification = {
    title: 'Notification Title',
    description: 'Notification Description'
  };

  const { container } = render(
    <NotificationProvider>
      <>
        <Notification />
        <Button notification={notification} />
      </>
    </NotificationProvider>
  );

  screen.getByRole('button', { name: /Button/i }).click();

  expect(container).toHaveTextContent(notification.title);
  expect(container).toHaveTextContent(notification.description);
});

it('renders success <Notification />', () => {
  const notification = {
    title: 'Notification Title',
    description: 'Notification Description'
  };

  render(
    <NotificationProvider>
      <>
        <Notification />
        <Button notification={notification} />
      </>
    </NotificationProvider>
  );

  screen.getByRole('button', { name: /Button/i }).click();

  expect(screen.queryByLabelText('Success')).toBeInTheDocument();
  expect(screen.queryByLabelText('Error')).not.toBeInTheDocument();
});

it('renders error <Notification />', () => {
  const notification = {
    title: 'Notification Title',
    description: 'Notification Description'
  };

  render(
    <NotificationProvider>
      <>
        <Notification />
        <Button notification={notification} isSuccess={false} />
      </>
    </NotificationProvider>
  );

  screen.getByRole('button', { name: /Button/i }).click();

  expect(screen.queryByLabelText('Error')).toBeInTheDocument();
  expect(screen.queryByLabelText('Success')).not.toBeInTheDocument();
});
