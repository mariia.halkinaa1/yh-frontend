import React from 'react';
import { render, screen } from '@testing-library/react';
import DefectRemovalTrend from './';

it('renders <DefectRemovalTrend />', () => {
  const data = {
    labels: ['Mon', 'Tue', 'Wen'],
    datasets: [
      {
        type: 'line' as const,
        data: [15, 20, 60]
      }
    ]
  };
  render(<DefectRemovalTrend data={data} />);

  expect(screen.getByRole('img')).toBeInTheDocument();
});

it('renders empty state', () => {
  render(<DefectRemovalTrend />);
  expect(screen.getByText('There is no data to display.')).toBeInTheDocument();
});
