export const options = {
  responsive: true,
  events: [],
  scales: {
    x: {
      grid: {
        display: false,
        drawTicks: false
      },
      border: {
        display: false
      },
      ticks: {
        autoSkip: false
      },
      // eslint-disable-next-line
      beforeBuildTicks: (axis: any) => {
        if (axis.max + 1 >= 40) {
          axis.options.ticks.maxRotation = 90;
          axis.options.ticks.minRotation = 90;
        } else {
          axis.options.ticks.maxRotation = 0;
          axis.options.ticks.minRotation = 0;
        }
      }
    },
    y: {
      border: {
        display: false
      },
      // eslint-disable-next-line
      beforeBuildTicks: (axis: any) => {
        const [{ data: backlog }, { data: opened }, { data: closed }] =
          axis.chart.data.datasets;
        axis.max = Math.max(...[...backlog, ...opened, ...closed]);
        if (axis.max > 20) {
          axis.options.ticks.stepSize = 20;
        } else if (axis.max > 10) {
          axis.options.ticks.stepSize = 5;
        } else if (axis.max > 5) {
          axis.options.ticks.stepSize = 2;
        } else {
          axis.options.ticks.stepSize = 1;
        }
      }
    }
  }
};
