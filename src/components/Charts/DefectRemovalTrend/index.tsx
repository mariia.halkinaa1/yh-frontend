import React from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  CategoryScale,
  BarController,
  BarElement,
  PointElement,
  LineController,
  LineElement,
  Legend,
  Title
} from 'chart.js';
import { Chart, ChartProps } from 'react-chartjs-2';

import { options } from './constants';
import styles from './style.module.scss';

ChartJS.register(
  LinearScale,
  CategoryScale,
  BarController,
  BarElement,
  PointElement,
  LineController,
  LineElement,
  Legend,
  Title
);

const DefectRemovalTrend = ({ data }: { data?: ChartProps['data'] }) => {
  return data ? (
    <div className={styles.defect_removal_trend}>
      <Chart type="line" options={options} data={data} />
    </div>
  ) : (
    <>There is no data to display.</>
  );
};

export default DefectRemovalTrend;
