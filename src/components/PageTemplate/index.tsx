import React, { useCallback, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import { PageLayout, Content, TopNavigation } from '@atlaskit/page-layout';

import Header from '../Header';
import MainContent from '../MainContent';
import PATH from '../../constants/routes';
import { useUserLocalStorage } from '../../hooks';

const PageTemplate = () => {
  const navigate = useNavigate();
  const { user } = useUserLocalStorage();

  const userName = useMemo(() => JSON.parse(user as string)?.name, [user]);
  const goToHome = useCallback(() => navigate(PATH.PROJECTS), [navigate]);

  return (
    <PageLayout>
      <TopNavigation height={60} isFixed>
        <Header goToHome={goToHome} userName={userName} />
      </TopNavigation>
      <Content>
        <MainContent />
      </Content>
    </PageLayout>
  );
};

export default PageTemplate;
