import React from 'react';

import SpinnerAK from '@atlaskit/spinner';

import styles from './style.module.scss';

const Spinner = () => {
  return (
    <div className={styles.spinner_container}>
      <SpinnerAK size="xlarge" />
    </div>
  );
};

export default Spinner;
