import React from 'react';

import { LogoutButton } from '../Buttons';
import { APP_NAME } from '../../constants/global';
import THEME_COLOR from '../../constants/themeColor';

import styles from './style.module.scss';

const Header = ({
  goToHome,
  userName
}: {
  goToHome: () => void;
  userName: string;
}) => {
  return (
    <header
      className={styles.header}
      style={{ backgroundColor: THEME_COLOR.PRIMARY }}
    >
      <h3 style={{ color: THEME_COLOR.WHITE }} onClick={goToHome}>
        {APP_NAME}
      </h3>
      <div className={styles.user_container}>
        <p style={{ color: THEME_COLOR.WHITE }} className={styles.user_name}>
          {userName}
        </p>
        <LogoutButton />
      </div>
    </header>
  );
};

export default Header;
