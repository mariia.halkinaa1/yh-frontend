import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';

import { APP_NAME } from '../../constants/global';
import Header from './';

it('renders <Header />', () => {
  const goToHome = jest.fn();
  const userName = 'User Name';

  render(<Header goToHome={goToHome} userName={userName} />, {
    wrapper: BrowserRouter
  });

  expect(screen.getByText(APP_NAME)).toBeInTheDocument();
});

it('renders user name in the <Header />', () => {
  const goToHome = jest.fn();
  const userName = 'User Name';

  render(<Header goToHome={goToHome} userName={userName} />, {
    wrapper: BrowserRouter
  });
  expect(screen.getByText(userName)).toBeInTheDocument();
});

it('renders Logout button in the <Header />', () => {
  const goToHome = jest.fn();
  const userName = 'User Name';

  render(<Header goToHome={goToHome} userName={userName} />, {
    wrapper: BrowserRouter
  });

  expect(screen.getByLabelText('Logout')).toBeInTheDocument();
});

it('simulates click events on the <Header /> title', () => {
  const goToHome = jest.fn();
  const userName = 'User Name';

  render(<Header goToHome={goToHome} userName={userName} />, {
    wrapper: BrowserRouter
  });

  screen.getByText(APP_NAME).click();
  expect(goToHome).toHaveBeenCalledTimes(1);
});
