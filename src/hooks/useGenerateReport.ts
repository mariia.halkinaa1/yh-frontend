import { useMutation } from 'react-query';

import reportApi from '../api/report';

const useGenerateReport = () => {
  const { isLoading, mutate } = useMutation(reportApi.generateReport);
  return { isGeneratingReport: isLoading, generateReport: mutate };
};

export default useGenerateReport;
