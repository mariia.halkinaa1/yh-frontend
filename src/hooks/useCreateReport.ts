import { useMutation } from 'react-query';

import reportApi from '../api/report';

const useCreateReport = () => {
  const { isLoading, mutate } = useMutation(reportApi.postReport);
  return { isCreatingReport: isLoading, createReport: mutate };
};

export default useCreateReport;
