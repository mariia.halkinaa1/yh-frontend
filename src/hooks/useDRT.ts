import { useQuery } from 'react-query';

import projectsApi from '../api/projects';
import { QUERY_KEYS } from './constants';

const useDRT = (id: string) => {
  const { isLoading, data } = useQuery(QUERY_KEYS.DRT, () =>
    projectsApi.getDRT(id)
  );

  return { isLoadingDRT: isLoading, drt: data };
};

export default useDRT;
