import { useQuery, UseQueryResult } from 'react-query';

import { SortOrderType } from '../components/Tables/types';
import { SortOrder } from '../components/Tables/constants';
import { REPORTS_PER_PAGE } from '../components/Tables/ReportsTable/constants';
import reportsApi from '../api/reports';
import { QUERY_KEYS } from './constants';

import type { IGetReportsRequest } from '../api/reports/types';

type TUseReportsParams = {
  page?: number;
  sortKey?: string;
  sortOrder?: SortOrderType;
  filter: {
    projectKey: string;
    projectJiraLink: string;
  };
};

type TUseReportsReturn = Pick<
  Awaited<ReturnType<typeof reportsApi.getReports>>,
  'results' | 'count'
> & {
  isLoading: UseQueryResult['isLoading'];
};

const formData = ({
  page,
  sortKey,
  sortOrder,
  filter: { projectKey, projectJiraLink }
}: TUseReportsParams) => {
  const data: IGetReportsRequest = {
    page_size: REPORTS_PER_PAGE,
    page,
    project_key: projectKey,
    jira_link: projectJiraLink
  };

  if (sortKey) {
    data.ordering = sortOrder === SortOrder.ASC ? sortKey : `-${sortKey}`;
  }

  return data;
};

const useReports: (
  isEnabled: boolean,
  params: TUseReportsParams
) => TUseReportsReturn = (isEnabled, { page, sortKey, sortOrder, filter }) => {
  const params = formData({ page, sortKey, sortOrder, filter });

  const { isLoading, data: { results = [], count } = {} } = useQuery(
    [QUERY_KEYS.REPORTS, page, sortKey, sortOrder],
    () => reportsApi.getReports(params),
    { enabled: isEnabled }
  );

  return { isLoading, results, count };
};

export default useReports;
