import { useQuery } from 'react-query';

import reportApi from '../api/report';
import { QUERY_KEYS } from './constants';

const useGenerationAvailable = () => {
  const {
    isLoading,
    data: { generation_available: isGenerationAvailable } = {}
  } = useQuery(QUERY_KEYS.GENERATION_AVAILABLE, reportApi.generationAvailable);
  return {
    isLoading,
    isGenerationAvailable
  };
};

export default useGenerationAvailable;
