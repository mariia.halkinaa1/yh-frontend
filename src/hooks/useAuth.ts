import { useNavigate } from 'react-router-dom';

import authApi from '../api/auth';
import PATH from '../constants/routes';
import { useAccessToken, useRefreshToken } from './useToken';
import { useUserLocalStorage } from './useLocalStorage';

function useAuth() {
  const navigate = useNavigate();
  const { accessToken, setAccessToken, removeAccessToken } = useAccessToken();
  const { refreshToken, setRefreshToken, removeRefreshToken } =
    useRefreshToken();
  const { user, removeUser } = useUserLocalStorage();

  const isAuth = !!(accessToken && refreshToken && user);

  const login = async (tokenId: string) => {
    const { access, refresh } = await authApi.logIn({ tokenId });
    setAccessToken(access);
    setRefreshToken(refresh);
    navigate(PATH.PROJECTS);
  };

  const clearCookies = () => {
    removeAccessToken();
    removeRefreshToken();
  };

  const goToLogin = async () => {
    navigate(PATH.LOGIN);
    removeUser();
    clearCookies();
  };

  const logout = async () => {
    navigate(PATH.LOGIN);
    await authApi.logOut();
    removeUser();
    clearCookies();
  };

  return { isAuth, login, logout, goToLogin };
}

export default useAuth;
