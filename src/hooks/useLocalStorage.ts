enum LocalStorageKey {
  USER = 'user'
}

const useLocalStorage = (key: LocalStorageKey) => {
  const getItem = () => localStorage.getItem(key);

  const setItem = (value: string) => {
    localStorage.setItem(key, value);
  };

  const removeItem = () => {
    localStorage.removeItem(key);
  };

  return {
    getItem,
    setItem,
    removeItem
  };
};

export const useUserLocalStorage = () => {
  const { getItem, setItem, removeItem } = useLocalStorage(
    LocalStorageKey.USER
  );

  const user = getItem();

  const setUser = (value: string) => {
    setItem(value);
  };

  return {
    user,
    setUser,
    removeUser: removeItem
  };
};
