import { useQuery } from 'react-query';

import generateReportApi from '../api/report';
import { QUERY_KEYS } from './constants';

const useProjectKeys = (isEnabled: boolean) => {
  const { isLoading, data = [] } = useQuery(
    QUERY_KEYS.PROJECT_KEYS,
    generateReportApi.getProjectKeys,
    { enabled: isEnabled }
  );
  return {
    isLoadingProjectKeys: isLoading,
    projectKeys: data
  };
};

export default useProjectKeys;
