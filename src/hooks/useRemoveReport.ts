import { useMutation } from 'react-query';

import reportApi from '../api/report';
import { QUERY_KEYS } from './constants';

const useRemoveReport = (id: string) => {
  const { mutate } = useMutation(QUERY_KEYS.REPORT, () =>
    reportApi.removeReport(id)
  );

  return { removeReport: mutate };
};

export default useRemoveReport;
