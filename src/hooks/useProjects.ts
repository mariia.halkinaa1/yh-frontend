import { useQuery, UseQueryResult } from 'react-query';

import { SortOrderType } from '../components/Tables/types';
import { SortOrder } from '../components/Tables/constants';
import { PROJECTS_PER_PAGE } from '../components/Tables/ProjectsTable/constants';
import projectsApi from '../api/projects';
import { QUERY_KEYS } from './constants';

import type { IGetProjectsRequest } from '../api/projects/types';

type TUseProjects = (params: {
  page?: number;
  sortKey?: string;
  sortOrder?: SortOrderType;
}) => Pick<
  Awaited<ReturnType<typeof projectsApi.getProjects>>,
  'results' | 'count'
> & {
  isLoading: UseQueryResult['isLoading'];
};

const formData = ({
  page,
  sortKey,
  sortOrder
}: Parameters<TUseProjects>[number]) => {
  const data: IGetProjectsRequest = {
    page_size: PROJECTS_PER_PAGE,
    page
  };

  if (sortKey) {
    data.ordering = sortOrder === SortOrder.ASC ? sortKey : `-${sortKey}`;
  }

  return data;
};

const useProjects: TUseProjects = ({ page, sortKey, sortOrder }) => {
  const params = formData({ page, sortKey, sortOrder });

  const { isLoading, data: { results = [], count } = {} } = useQuery(
    [QUERY_KEYS.PROJECTS, page, sortKey, sortOrder],
    () => projectsApi.getProjects(params)
  );

  return { isLoading, results, count };
};

export default useProjects;
