import { useContext } from 'react';
import {
  useQuery,
  useMutation,
  useQueryClient,
  UseQueryResult
} from 'react-query';

import settingsApi from '../api/settings';
import NotificationContext from '../context/notificationContext';
import successMessages from '../helpers/messages/successMsg';
import { QUERY_KEYS } from './constants';

type TUseSettings = () => {
  isLoading: UseQueryResult['isLoading'];
  settings: Awaited<
    ReturnType<typeof settingsApi.getSettings>
  >['results'][number];
};

export const useSettings: TUseSettings = () => {
  const { isLoading, data: { results = [] } = {} } = useQuery(
    QUERY_KEYS.SETTINGS,
    settingsApi.getSettings
  );
  return { isLoading, settings: results[0] };
};

export const useTimezones = () => {
  const { isLoading, data } = useQuery(
    QUERY_KEYS.TIMEZONES,
    settingsApi.getTimezones
  );
  return { isLoadingTimezones: isLoading, timezones: data };
};

export const useSettingsUpdate = () => {
  const { onSuccess } = useContext(NotificationContext);
  const queryClient = useQueryClient();

  const { mutate } = useMutation(settingsApi.putSettings, {
    onSuccess: async (settings) => {
      onSuccess(successMessages.UpdateSettings);
      queryClient.setQueryData(QUERY_KEYS.SETTINGS, {
        isLoading: false,
        results: [settings]
      });
      await queryClient.invalidateQueries([QUERY_KEYS.PROJECTS]);
    },
    onSettled: async () => {
      await queryClient.invalidateQueries([QUERY_KEYS.GENERATION_AVAILABLE]);
    }
  });
  return { mutate };
};
