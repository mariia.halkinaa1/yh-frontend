import { useQuery } from 'react-query';

import reportApi from '../api/report';
import { IQAReport } from '../api/report/types';
import { QUERY_KEYS } from './constants';

const useReport = (id: string) => {
  const { isLoading, data } = useQuery(QUERY_KEYS.REPORT, () =>
    reportApi.getReport(id)
  );

  return { isLoading, report: data as IQAReport };
};

export default useReport;
