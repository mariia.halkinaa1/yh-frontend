export const QUERY_KEYS = {
  GENERATION_AVAILABLE: 'generationAvailable',
  SETTINGS: 'settings',
  TIMEZONES: 'timezones',
  PROJECT_KEYS: 'projectKeys',
  PROJECTS: 'projects',
  PROJECT: 'project',
  REPORTS: 'reports',
  REPORT: 'report',
  DRT: 'defectRemovalTrend'
};
