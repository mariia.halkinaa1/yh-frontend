import { useCookies } from 'react-cookie';

enum TOKEN {
  ACCESS_TOKEN = 'accessToken',
  REFRESH_TOKEN = 'refreshToken'
}

const useToken = (key: TOKEN) => {
  const [cookies, setCookie, removeCookie] = useCookies([key]);

  const setToken = (authToken: string) => setCookie(key, authToken);

  const removeToken = () => removeCookie(key);

  return {
    token: cookies[key],
    setToken,
    removeToken
  };
};

export const useAccessToken = () => {
  const { token, setToken, removeToken } = useToken(TOKEN.ACCESS_TOKEN);

  return {
    accessToken: token,
    setAccessToken: setToken,
    removeAccessToken: removeToken
  };
};

export const useRefreshToken = () => {
  const { token, setToken, removeToken } = useToken(TOKEN.REFRESH_TOKEN);
  return {
    refreshToken: token,
    setRefreshToken: setToken,
    removeRefreshToken: removeToken
  };
};
