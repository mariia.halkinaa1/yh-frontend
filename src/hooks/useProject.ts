import { useQuery } from 'react-query';

import projectsApi from '../api/projects';
import { IProject } from '../api/projects/types';
import { QUERY_KEYS } from './constants';

const useProject = (id: string) => {
  const { isLoading, data } = useQuery(QUERY_KEYS.PROJECT, () =>
    projectsApi.getProject(id)
  );

  return { isLoadingProject: isLoading, project: data as IProject };
};

export default useProject;
